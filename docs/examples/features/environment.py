from mestr.utils.base_env import (mestr_before_all, mestr_after_scenario, 
                                  mestr_before_feature, mestr_before_scenario)


@mestr_before_all
def before_all(context: Context)->None:
    """function executed before all tests are run""" 
    pass 


@mestr_before_feature 
def before_feature(context: Context, feature: Feature) -> None: 
    """function executed before all feature""" 
    pass 


@mestr_before_scenario 
def before_scenario(context: Context, scenario: Scenario) -> None: 
    """function executed before each scenario""" 
    pass 


@mestr_after_scenario 
def after_scenario(context, scenario):
    """function executed after each scenario""" 
    pass


@mestr_after_feature
def after_feature(context, feature):
    """function executed after each feature"""
    pass
