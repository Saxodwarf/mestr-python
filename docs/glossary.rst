Glossary
========

DUT
~~~

Device under test

Test platform
~~~~~~~~~~~~~

| Platform from wihich the tests are run.
| We are currently using :

-  a Beaglebone Black,
-  a USB to ethernet adapter, to provide a second network interface,
-  a Relay module board, to control the power supply of the DUT,
-  a CAN cape (that we just received), to communicate through the CAN
   bus.

`Behave <https://behave.readthedocs.io/en/latest/>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Behavioural test framework, writen in python.

The tests are defined in a language called Gherkhin, very close from
natural english. They are implemented in Python.

Behave supports **Tags**

The test definition is located in a **feature** file, which contains at
least one **scenario**. A test scenario is a sequence of **steps**. The
steps are implemented in Python in a **step** file.

Feature
~~~~~~~

The tests definitions are divided in feature files, which contains all
of the tests dedicated to this feature (for instance, a ``dhcp.feature``
file that contains all the tests definitions to test the DHCP client of
the DUT).

These feature files contains one or more **scenario**.

Scenarios
~~~~~~~~~

A scenario is the equivalent of a test case. It is written like a story,
in the less technical possible way. It must be understandable by every
stakeholder, and should reflect the DUT specification.

Tags
~~~~

Tags can be specified at the feature and the scenario level, in the
feature file.

By default, tags are used for grouping and selecting tests run by
behave. We extended the behavior to require specific manager for the test
execution. To do so, just write tag with name of the manager requested on
the scenario or feature.

::

   @manager_name

It allows Behave to run only the tests for which we have the appropriate
interfaces.

These tags can also be used to indicate a severity level (blocker,
critical, normal, minor, trivial).

| To execute groups of tests, simply put their tag in the **Tag** list
  of the ``config.yml`` file.
| If a tag list is set in the ``config.yml`` file, only the tests
  carrying one (or more) of the list tags will be executed.

config.yml
~~~~~~~~~~

Test setup description file.

Describes the test environment, and contains :

At least :
^^^^^^^^^^

| A list of the interfaces, that we call “managers”, between the DUT and
  the Test Platform.
| Those managers will be used during the tests. The idea is to separate
  the tests definition from the available hardware configuration.

Optionally :
^^^^^^^^^^^^

-  A list of tags
-  The path to the tests definitions to use (the Features)
-  A stage, to be used as a prefix for the step directory and the
   environment file
   (it allows to use a different test implementation without modifying
   the test definitions)

Manager
~~~~~~

To each manager corresponds one python class, which stores the needed
parameters -for instance, a serial connection would need (at least) a
baud rate and a UART number-, and the methods that can be used to
interact with it.

The managers are defined like so :

.. code:: yaml

       - type: type_name # used to find the related class.
       - name: manager_name # name to refer to it in the feature files (test definition file)

The managers can also have other arguments, which will be passed to the
``__init__`` method of their respective class. Those arguments are given
in the same way :

.. code:: yaml

       - opt_arg1: arg_content1 # first optionnal argument
       - opt_arg2: arg_content2 # second optionnal argument

`Allure <https://docs.qameta.io/allure/>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A reporting framework.

To organize our test reports, we use the Allure framework. It generates
a nice overview of the test results.

There is a behave formatter that makes the report generation easy.

In the steps implementation, files can be attached to the allure report.
These files will be available in the test report, through the web
interface. This is quite handy for keeping log files, or to keep records
for further analysis.

It can also be integrated to Jenkins, to automatically create a report,
and keep track of the test history.
