Areas for improvements
======================

For now, Mestr is capable of running tests on a specific DUT.
However, there is still a lot of room for improvements.

A software update mechanism
---------------------------

The most important missing part to make the proof of concept really
operational is a way to update the software of the DUT. This is completely
specific to the device, and this is why it was not a priority for us, as this
feature would need to be adapted for every different DUT.

However, several things have been done and some others could be done to
make this feature easier to implement.

Things that have been done:
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The DUT's power alimentation and serial port communication can already be controlled.
A lot of devices can be updated by interrupting the bootloader using the serial port,
to download a specific image, and it can be done already
Moreover, Dnsmasq, which is used as a DNS and DHCP server, can also provide a tftp server,
that could be used to host an image for the DUT.


Things that could be done:
~~~~~~~~~~~~~~~~~~~~~~~~~~

There are some features related to the software update mechanism that
could be implemented.

A hook mechanism:
^^^^^^^^^^^^^^^^^^

In MESTR, create a hook that would trigger the
software update mechanism.

This hook could call an external program or a python script that would
be written by the DUT’s creator to update it. This update mechanism
could, of course, use the manager system to communicate with the
prototype.

To define this hook, we must define where and when it will be called.
We have several options here, with their pro and cons:

- In the main function:
    If the hook call is made here, the DUT will be updated before every session
    of tests. However, even if there is a problem at behave's level, it will be
    reseted.

- In ``before_all``:
    That way, the device will be updated and reseted for every run of behave.
    It has pro and cons :

    - Pro :
        It is fast, as the device is only flashed once.
    - Con :
        It does not reset the DUT between each tests. Therefore we can not be
        sure of the state of the DUT for all the tests.


- In ``before_scenario``:
    The device will be flashed before each test case.

    - Pro :
        The DUT is in a clean state before each of the tests, ensuring that we
        known what is happening on the DUT.
    - Con :
        It adds a lot of time to flash and reboot th device between each tests.

    
- As a fixture:
    The hook could be called as a fixture. That way, we could decide for each
    tests wether we want the DUT to be flashed before or not.

An external tool triggered by the CI:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The software update mechanism could be triggered by a CI tool before running MESTR.

The DUT would only be updated once, but it would completely dissociate the (device
specific) update of the DUT from the test software.

In this situation, the software update would be done either by an external tool with
no relation with MESTR, or could maybe be a tool that uses the config_parser, and
managers defined for MESTR in its implementation.

The last option would allow to update the hardware configuration without breaking the
DUT update tool.

A stable image in case of failure:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Even if we try to clean image between tests, a test can create an instability so that the next will fail.
To detect such problem, if a test fail, we could try to upload a stable image and then launch the test again.
If it's still failing, we can consider a regression. On the other hand, if the test pass, we could consider that the previous
test is not stable.


A golden image for integrity:
-----------------------------


Have a golden image available, to make sure that the hardware
configuration is working fine (and create the platform to host and
download this golden image).

In case of repeated test failure, one could load a known-to-be-working
image on the DUT using a software update mechanism, to check that the
problem does not come from the hardware configuration. It could be
combined with the integrity tests, to give a full overview of the
hardware health.

A reservation mechanism
-----------------------

As of today, Mestr is controlled by a ci tool. However, since it is
directly using a physical device, it can not be used by two
different entities at the same time. 

For now, a lock mechanism is used : if an instance of MESTR is running, no
other instance can be run. It prevents interferences between tests run by
the CI tool and tests run by a human.

It would be interesting to add a reservation system to this mechanism,
allowing a developer to take control of the DUT in place of the ci tool,
in order to run some tests or do some researches.

Handling analog inputs and/or outputs
-------------------------------------

Analog inputs/outputs are tricky. Indeed, the interface needs to be
chosen carefully according to what is being monitored.

In our situation, the bbb has some built-in analog inputs. However, they
are only rated for a maximum input of 1.8V. We had access to a prototype
delivering an analog output between 0 and 10V. Therefore it was
impossible to directly test the output of the prototype using our
platform. There may also be some restrictions related to the precision
of the interface.

We were not able to use an analog input, but the best way to do so seems
to be using an ADC or DAC chosen specificaly according to the tested
prototype, and to handle it through I²C.

Develop some custom hardware
----------------------------

The hardware setup used to develop Mestr is still a bit messy.

We have a lot of wires going everywhere. It is inevitable, to connect to
a DUT. However, some of it could be avoided. For instance, the 4-relay module
needs 7 wires to be controlled: one for ground, 4 to control the
relays, one +3.3V to power the command circuit and one +5V to power the
relays.

It could be reduced by developing our own piece of hardware. It would
contain all the basic additional hardware, and could integrate some
relays, a usb hub and usb-to-ethernet adapter, a CAN transceiver.

It would not be necessarily based on a Beaglebone black. If an other
board could fit better, or bring interfaces unavailable on the bone,
and impossible to add, like gigabit ethernet or USB3.0, it could be used instead.

We made some researches about what would be the needed hardware, and made 2
propositions:

One of them is based on the Beaglebone Black, as is the current
setup, and the other is based on a NanoPi Neo.

The Beaglebone Black based setup:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
It has the advantage of necessiting almost no adaptation on the software side.
Plus, the BBB has a lot of GPIOs, a built in CAN controller ...

However, it has no USB 3.0 nor 10/100/1000mbs Ethernet interface, which can be
a limitation. Moreover, our extension board would need to be plugged to the usb
port of the Beaglebone Black, as it is not exposed to a header...

Here is the block diagram of the setup we imagined, based on the Beaglebone Black:

.. image:: ../imgs/soc_BBB.png
    :align: center

The NanoPi Neo Core based setup:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This board has all of its interfaces exposed to header pins, which makes it easy
to include in a baseboard.

However, some adaptations would be needed (see :ref:`portability <portability-ref>`)
to use some specific libraries.

It has less GPIOs, and would need an additional can controller, but it supports
USB 3.0, and has a gigabit Ethernet interface.

Here is the block diagram of the setup we imagined, based on the NanoPi Neo Core2:

.. image:: ../imgs/soc_nanopi.png
    :align: center

Create some kind of state machine
---------------------------------

The majority of the tests run in the DUT are dependent of some kind of state. As an
example, testing a web server on the DUT will only work in if the device is booted and a
network connection is up. If the dependency between being booted and having a network
connection up is quite obvious, most of the dependencies depend of the device, it’s why
we didn’t implement such a mechanism yet. It also means that for now we have to say
explicitly that the device has to be booted and then that network has to be up at
the beginning of a web server test.

An interesting feature would be to be able to define the dependencies between available
managers state, for example by adding field in the manager definition. For example, we
could define a **dependency** field that would contain a list with manager name and state necessited to use this manager.

.. code-block:: yaml

    Managers:
        -
            type : relay
            name : power
        -
            type : network
            name : network
            dependency :
                power : up
        -
            type : web
            name : http
            dependency :
                network : up
        -
            type : ssh
            name : ssh
            dependency :
                network : up


An other improvement could even be to guess the current state of the DUT and to be
able to go to the expected state from the current one without passing by all the steps
from the first one.

Difficulties
~~~~~~~~~~~~~

The main problem to implement this feature is that most of the managers are dependent of a booted DUT. Nevertheless, knowing if a DUT is booted is device dependant so we cannot implement a generic mechanism for it.


Get extra information in case of failure
----------------------------------------

Even if we add extra logging informations in our tests, it could be not
enough in case a failure. A relatively easy feature would be to specify
files and/or command to get extra informations like messages from the
DUT’s kernel in case of test failure. Those files would be attached to a
manager and be called if the test fails and the manager is involved.

However, it can become a bit tricky if the DUT is unresponsive (in which
case such informations should be recovered is a less trivial way, directly from the prototype
memory for instance).


Find a reliable way to simulate network cuts.
---------------------------------------------

During the project, we used to simulate network cuts by turning down the interface that was connected to the DUT. However, when turned down, the usb to ethernet adapter is not actually *powered* down. Therefore, the DUT does not realise that it has been disconnected.

So tried to use the built in ethernet interface as the LAN interface. And it can actually be powered down. Everything worked well, until we tried it with an other prototype. This new prototype inexplicably refused to work with the built interface.

Since we had a choice between not making network tests **at all**, or making network tests without network cuts, we chose the second option and are now using the usb to ethernet adapter as the LAN interface.


However, it would be good to be able to simulate network cuts again.

We tried disabling the USB interface, but it is not powered down when disabled...
