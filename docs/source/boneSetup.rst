Beaglebone Setup
============================================================================================

Current supported hardware
------------------------------------------
 
 * `BeagleBone Black Rev C <http://beagleboard.org/black>`__
 * `4 relays module <https://www.sainsmart.com/products/4-channel-5v-relay-module>`__
 * `usb/ethernet hub <https://www.adafruit.com/product/2992>`__

BeagleBone initialization
------------------------------------

There is 2 main options for the operating system:

    - The recommended Debian "IOT" image, which is shipped with the Beaglebone Black:
        You can get it `here <https://beagleboard.org/latest-images>`__.
    - The console Debian image, which is much lighter (but harder to find):
        It is a testing snapshot, so it might be a bit less stable.
        You can get it `here <https://elinux.org/Beagleboard:BeagleBoneBlack_Debian#Stretch_Snapshot_console>`__.

To setup the platform environment :

1. Flash an SD card with the chosen image.
    
You can uncomment the following lines from the file  ``/boot/uEnv.txt``. It’s not absolutely needed
but you shouldn’t use this capes and it uses some GPIO pins

::

   #disable_uboot_overlay_video=1
   #disable_uboot_overlay_audio=1
   #disable_uboot_overlay_wireless=1

2. Power up the Beaglebone.

3. **(Optional)** Update uboot

If you have troubles to use Mestr it can be due to uboot. You can upgrade **uboot** and **initrd** with the provided scripts : 

.. code-block:: bash

    ./opt/scripts/tools/developers/update_initrd.sh
    ./opt/scripts/tools/developersupdate_bootloader.sh

.. note:: **Flashing the eMMC**
   
   You can decide to not run the OS not from the SD card but directly from the eMMC.
   To do so,  Uncomment the following lines in the file ``/boot/uEnv.txt`` :
    
    .. code-block:: bash

        #cmdline=init=/opt/scripts/tools/eMMC/init-eMMC-flasher-v3.sh # auto flashing eMMC

    At the beginning, we had some issues with launching device tree from the sd card. It seems fixed now but it was not possible to have access to universal-io-cape. This
    cape is used for configuring GPIO pins in the project.

distribution cleanup
^^^^^^^^^^^^^^^^^^^^
If you use the recommended "IOT" distribution, you may want to remove some of the built in services.

You can at least stop the bone server by disabling
``apache2 bonescript.socket bonescript.service`` services.

You can also remove these unused packets :

.. code-block:: bash

   apache2 apache2-* bb-node-red-installer  bonescript bluetooth  firmware-iwlwifi  nodejs  pastebinit  c9-core-installer ardupilot* librobotcontrol

Mestr setup
--------------------------------------------

Requirements
^^^^^^^^^^^^

Before using mestr project, you need to install following dependencies :

 * gitlab-runner (`installation instructions <https://docs.gitlab.com/runner/install/linux-repository.html>`__)
 * tcpdump
 * python3.7
 * pipenv for python3.7


Install python 3.7 on Debian
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On Debian, Python 3.7 is currently only available in the testing repository.

You can either cross compile Python 3.7 from source, or install it from the testing distribution.

To install it from testing, you must:

Add the testing repository to your ``/etc/apt/source.list`` file, like so:

.. code-block:: bash

    ##### testing:

    # Testing repository - main, contrib and non-free branches
    deb http://deb.debian.org/debian testing main non-free contrib
    #deb-src http://deb.debian.org/debian testing main non-free contrib

    # Testing security updates repository
    deb http://security.debian.org/ testing/updates main contrib non-free
    #deb-src http://security.debian.org/ testing/updates main contrib non-free

Then, to avoid any unwanted conflict, adjust the package repository priority.
If your ``/etc/apt/preferences`` file is empty (or doesn't exist yet), just put the following into it:

.. code-block:: bash

    Package: *
    Pin: release a=stable
    Pin-Priority: 700

    Package: *
    Pin: release a=testing
    Pin-Priority: 400

You can check the ``apt_preferences`` man page for more informations about apt's preference mechanism.

Finally, you can run

.. code-block:: bash

    sudo apt update
    sudo apt -t testing install python3

to install Python 3.7. The ``python3`` command wil now run the 3.7 version of Python, but the 3.5 version can still be run using ``python3.5``.

Install pipenv
^^^^^^^^^^^^^^^^^^

Once python3.7 is installed, you can install ``pipenv`` through **pip** : 

.. code-block:: bash

    sudo pip3 install pipenv


Allow the user **gitlab-runner** to run the tests (without being root)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To use the GPIO ports of the beaglebone, the **gitlab-runner** user must be added to the following groups : ``dialout i2c gpio pwm spi``
   
To do network tests, **gitlab-runner** must be able to run the ``tcpdump`` and ``ip`` commands, to capture packets and simulate network cuts.
The easiest way to do so without giving **gitlab-runner** root privileges is to configure the file capabilities of the binaries, and to restrict
their execution rights to a certain group to limit the potential risks.

You can just run the ``runner_rights_config.sh`` script to configure all of this.

Alternatively, you can do it by hand :

 - Add gitlab-runner to the needed groups to use the capabilities of the Beaglebone black:

    .. code-block:: bash

        usermod -a -G i2c,gpio,pwm,spi,dialout,eqep gitlab-runner

 - Allow ``tcpdump`` and ``ip`` commands to be run without root privileges:

    .. code-block:: bash

        #Install the libcap2-bin package
        sudo apt update && sudo apt install libcap2-bin

        # Create a "network" group
        sudo groupadd network

        # Add the gitlab-runner user to this group
        sudo usermod -a -G network gitlab-runner

        # Change the tcpdump and ip group to "network"
        sudo chgrp network /usr/sbin/tcpdump
        sudo chgrp network /bin/ip

        # Restrict the execution right to the member of "network" and the file owner
        sudo chmod 750 /usr/sbin/tcpdump
        sudo chmod 750 /bin/ip

        # Set the needed capabilities
        sudo setcap cap_net_raw,cap_net_admin=ep /usr/sbin/tcpdump
        sudo setcap cap_net_admin=ep /bin/ip

.. note:: Only the member of the group "network", and the root user will be able to run the tcpdump and ip command. The members of the group "network" will be able to mess up with the network configuration, using the ``ip`` command, whithout the need of ``sudo``. Be careful with who you give these rights.

Use the CAN bus
^^^^^^^^^^^^^^^^

To configure the CAN bus, you need to :

 - configure the CAN network interface.
 - reboot the test platform.
 
To configure the CAN interface on boot, either add these lines to ``/etc/network/interfaces`` :

.. code-block:: bash

    # Canbus interface :
    auto can1
    iface can1 inet manual
            #bitrate 125000
            down /sbin/ip link set $IFACE down
            up /sbin/ip link set $IFACE up type can bitrate 125000

Or put those lines in a ``/etc/network/interfaces.d/CAN`` file, and add the line:

.. code-block:: bash

    source /etc/network/interfaces.d/*

to ``/etc/network/interfaces``. This last approach allows to easily remove or change the configuration of the can bus, by making it easy to locate.


.. note:: You must set the bitrate to fit the need of your DUT

The Beaglebone's pin will be configured automatically when using the :class:`CanManager <mestr.managers.can_man.CanManager>` class.
You can find more detailed informations about the Beaglebone's CAN bus :ref:`here <can_bus_ref>`.

.. note:: You will need a CAN tranceiver to send and receive data.

Register gitlab-runner
^^^^^^^^^^^^^^^^^^^^^^

To configure the gitlab-runner, you need a token from Gitlab, then run :

.. code-block:: bash

     sudo gitlab-runner register

and follow the instructions. The runner executor is ``shell``.

more information can be found
`here <https://docs.gitlab.com/runner/register/index.html>`__

.. note:: To select the runner on the beaglebone when a gitlab job is triggered, you have to associate a tag to the runner that you will specify in the job. We are currently using the tag **mestr**

Set up the network
^^^^^^^^^^^^^^^^^^^^^^^^
If your DUT needs a DHCP server to get an IP address, you will need to install and configure one.
In the Beaglebone Black image, Dnsmasq is already installed. You only have to configure it as you want.
If your device needs an internet access, you will also need to set up ip forwarding and NAT.

For now, only IPv4 is supported.

To do so easily, we have some scripts:

Configure the DNS and DHCP server.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The easiest way is to just run the ``configNetwork.sh`` script which can be found in the repository under ``beagleBone/networkScript/network/``. All the ethernet interfaces must be active for the Beaglebone's network manager to configure them. The easiest way is to connect them one to the other and to use the serial console to run the script.

If you have two or more ethernet interfaces, you will be prompted for the one to use as lan and the one to use at wan. If you have only one, you will be prompted for its role (lan or wan).

If you want to give a specific IP address to the DUT, edit the "config.cfg" file located in the same directory as the ``configNetwork.sh`` script.
Uncomment these lines:

.. code-block:: bash

    # staticDutIP=0
    # dutIPAdress=192.168.0.131
    # dutMacAdress=

Set the dutMacAdress parameter to the MAC address of the DUT, and staticDutIP to 1, like so:

.. code-block:: bash

    staticDutIP=1
    dutIPAdress=192.168.0.131
    dutMacAdress=FF:FF:FF:FF:FF:FF

For more information about the possibilities of the script, or about the default parameters, please see :ref:`here <network_conf_ref>`.

Configure routing
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The easiest way to set up the NAT and the IP forwarding is to run the ``setupRouting.sh`` script located in ``beagleBone/networkScript/routing/``.

The script can be called like this:

.. code-block:: bash

    ./setupRouting.sh -w wanInterface -l lanInterface [-pn]

wanInterface and lanInterface must be the name of, respectively, your wan and lan interface.

The -p parameter can be used, in order to keep the configuration after rebooting. It will install the iptables-persistent package, save the iptables rules, and add the line ``net.ipv4.ip_forward = 1`` in ``/etc/sysctl.conf``. During the iptables-persistent installation, you will be asked if you want to save the current iptables rules. It must be answered for the install process to complete (although the answer has little importance, as the rules are always saved by the script in persistent mode. The file created by iptables-persistent will be overriden). The important thing to remember is that the script must be run by a human if iptables-persistent is being installed.

If iptables-persistent is already installed on your system, the -n parameter prevents the scripts from trying to install it (it can make you gain some time). The rules will still be saved.

.. warning:: Please be aware that in persistent mode, all the current iptabes rules will be saved (and restored after rebooting). Therefore by default, this script will add some rules but save the new **and the former ones**. If you want to clear the existing set of rules (to exchange the wan and the lan ethernet interfaces for instance), you can use the -c parameter.

Configure port forwarding.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is unlikely that you want to enable port forwarding all the time.
If you want to configure it, please see :ref:`here <port_forw_ref>`.

It could be made reboot-persistent by running the ``setupRouting.sh`` script without the "-c" parameter after this one.

launch tests
^^^^^^^^^^^^

To launch tests, just run test pipeline from Gitlab. Run at least one
time the stage pipenv before running stage test .
