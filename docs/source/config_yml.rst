.. _config-yml-ref:

Config.yml file
====================

The ``config.yml`` describes all the parameters of MESTR, except for the managers definitions (see :ref:`here <managers-yml-ref>`)

Feature
-------------
    
**Optional** :  features file or directory to execute (default : ``tests/features`` directory)

Examples : 

.. code-block:: yaml

    Features : feature/boot.feature
    # look for "feature/boot.feature" file
    
    Features : DUT/features
    # look for "DUT/features" directory

More informations about tag :ref:`here <tags-ref>`.
    
Reports
---------------

**Optional**: Can contain 2 type of field. 

- *output* [unique]: File or directory to write the reports.
    If it's a file, generated attachement will be saved in the directory containing the file.
- *Formatter* : Behave formatter for the report generation.
    List is available by the ``behave --format help`` command
                        
Example :

.. code-block:: yaml
    
    Reports : 
      output: ./prototype_tests/reports/
      formatter: allure_behave.formatter:AllureFormatter
    # this generate reports with allure format in the directory : ./prototype_tests/reports/


Stage
-------------

**Optionnal** :  prefix used for step directory and environment file used by behave.

Example : 

.. code-block:: yaml
    
    Stage : stage
    # will look for "stage_steps" directory and "stage_environment.py" file


Tags
------------

**Optional** :  extra tags you want to set.

- Tag on the same line separated by a ``,`` are considered separated by ``OR``
- Tag on separates lines are considered separated by ``AND``
- Tag with a ``~`` in front are evaluated as ``NOT tag``

Example : 

.. code-block:: yaml

    Tags : 
        - foo, ~bar
        - zap
    # is equivalent to  : (@foo OR NOT @bar) AND @zap
    

Filters
------------

**Optional** :  Filters for the feature files.

There is two type of filters, defined using python-flavoured regexes:

- The **include** filter: only feature containing the regex are run
- The **exclude** filter: only feature NOT containing the regex are run.

If both filters are defined, only feature containing the include regex AND not containing the exclude regex are run.

Example : 

.. code-block:: yaml

    Filter : 
        include: RE
        exclude: GEX
    # This will select feature files whose names contain RE and does not contain GEX.

.. note:: If a path is given for the feature files, it is part of the name and must be
    taken into account while writing the filters.
    
Managers
-------------------

**Optional** :  Path to the file with the managers configurations.  (default : ``/home/gitlab-runner/managers.yml``)

.. code-block::yaml

    # use /home/gitlab-runner/hardware.yml file
    Managers: /home/gitlab-runner/hardware.yml


.. _managers-yml-ref:

Managers.yml file
====================

This file should be hosted on the platform. It specify wich managers are present and
should only be updated when you update your hardware installation.
    
Managers
----------------

**Required** : list of managers availables
    
each manager has :

- **type** : use to find the related class. Type  is defined by the method :meth:`man_type <mestr.managers.manager.Manager.man_type>`
- **name** : name to refer to it in the feature files
- **extra args** : args that are passed to the constructor of the class

Example : 

.. code-block:: yaml
    
    Managers : 
        - # manager for serial connection
            type: serial  
            name : debug 
            target : 2
            baudrate : 115200

        - # relay manager to power up the DUT
            type : relay 
            name : power  
            gpio : P9_15
            mode: ACT_LOW
