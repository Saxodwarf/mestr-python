Implement new Manager
----------------------------

In **Mestr**, managers are classes in charge of managing an interface between the **test platform** and the **DUT**. This interface can be either a communication protocol (serial, ssh, https...), a sensor (light sensor, microphone...) or an actuator (relay, motor...).

**Managers** implementations are subclasses of ``Manager``. As subclasses, they must at least redefine the method :py:meth:`man_type() <mestr.managers.manager.Manager.man_type>`. This method refers to the ``type`` parameter for each manager in the ``managers.yml`` file.


To be used by mestr, the manager has to be declared in the ``__init__.py`` file of the managers directory. Simply add the name of the module containing the manager to the list.

You should also define the attribut :attr:`_available_states <mestr.managers.manager.Manager._available_states>`. This attribut  is a dictionary with states name as keys and  function to putthe manager in the given as value.


.. code-block:: python
    
    from mestr.managers.manager import Manager
    
    class FooMan(Manager):
        """ My awesome Foo manager"""
        def __init__(self):
            self._available_states = {"s1", "s2", "s3", "s4"}
            self._state = "s1"
            
        @staticmethod
        def man_type():
            return "foo"

            
Already defined functions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The base class manager already implements some helpers functions to easily fit with existing steps implementations.

.. automodule:: mestr.managers.manager
    :noindex:
    :members:
    :undoc-members:
    :show-inheritance:

Furthermore, there is some conventions that a new manager class should follow in order to be as compliant as possible with other Managers and make tests more generic.

Communication protocol
~~~~~~~~~~~~~~~~~~~~~~~~~~~

:function login():
    login to the remote point
        :return: boolean value according to the success or not of the login operation

:function logout():
    logout from the remote point
        :return: boolean value according to the success or not of the logout operation
        
:function send(data):
    send data through the manager.
        :param data: data to be sent by the manager through the interface
        :return: boolean value according to the success or not to of the sending operation
    
:function read():
    read one message/line.
        :return: an object containing the message/line read and that has an __str__ method
        
:function expect(value, timeout):
    read until it matches value. The expected behavior is supposed to be close to the pexpect library. It returns a number which is the index of the sequence that match in value. It stores the message that matches in an ``after`` attribute and all the previous value (as a string or a list) in a ``before`` attribute
        :param value: the values to match. 
        :type value: Union[AnyStr, Pattern, List[Union[AnyStr, Pattern]]]
        :return: index of the sequence that match.

:function flush():
    flush the buffer if it exists
        :return: True if success, false otherwise

Sensor
~~~~~~~~~~~~

:function get_value():
    read the current value of the sensor
        :return: value of the sensor
        
Actuator
~~~~~~~~~~~~

:function set_value(val):
    set the value to the actuator
        :param val: value to set to the actuator
        :type val: int
        :return: nothing
