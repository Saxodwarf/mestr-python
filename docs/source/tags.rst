.. _tags-ref:

Tags management
====================

**Mestr** uses **behave** Tag system to perform specific actions. As a consequence, you can do everything as with **behave** (`details <https://behave.readthedocs.io/en/latest/gherkin.html#tags>`_). Besides, it add two features.

Managers tags
----------------------

A manager tag is a tag with the following format : 

.. code-block:: gherkin
    
    manager.<name>
    
The variable `<name>` is the name set in the ``config.yml`` file. Adding a manager tag on top of a feature or a scenario means that this manager is required. If it not present in the ``config.yml`` file, the test will be skipped. Adding this tag will also tell mestr to log some informations about the manager actions and to flush it at the end of the scenario.

Integrity tag
---------------------

``integrity`` is a reserved tag to define a feature containing test about physical integrity of the hardware. The aim of this test is not to test the prototype itself but to assure that the manager is working properly before doing any test on it.

To be run, the tag ``integrity`` has to be set on a feature. Each integrity test will be linked to the first manager tag on the scenario. If the test fails, manager is considered as not working and tests that depend of it will be skipped.

If you skip an integrity test, the manager is considered as working.

Example : 

.. code-block:: gherkin

    @integrity
    Feature: testing hardware integrity

        @manager.network # manager to test integrity
        Scenario: DHCP integrity
            Given PROTO has to be booted # device specific step
            Then 'network' should be 'up'
        
        @manager.api # manager to test integrity
        @manager.network
        Scenario: api integrity
            Given 'network' has to be working
            And PROTO has to be booted
            Then 'api' is able to reach the web server

            
.. note::

    You can set several features with the tag integrity but in this case, we don't manage wich one is used first. According to behave behavior, it follow alphabetical order. Nevertheless, it's safer to use only one feature for integrity or being sure that there is no dependency between them.
