.. _docid.steps.mestr.steps.can_steps:
.. index:: mestr.steps.can_steps

======================================================================
mestr.steps.can_steps
======================================================================

:Module:   mestr.steps.can_steps
:Filename: mestr/steps/can_steps.py

Some CAN specific  steps

Step Overview
=============


==================================================================== ===== ==== ==== ====
Step Definition                                                      Given When Then Step
==================================================================== ===== ==== ==== ====
When I send the CAN packet '{packet}' from the DUT using '{manager}'         x           
Then I should receive '{packet}' on '{name}'                                      x      
When I listen for CAN packets on the DUT using '{manager}'                   x           
Then I should receive '{string}' on the DUT using '{manager}'                     x      
==================================================================== ===== ==== ==== ====

Step Definitions
================

.. index:: 
    single: When step; When I send the CAN packet '{packet}' from the DUT using '{manager}'

.. _when i send the can packet '{packet}' from the dut using '{manager}':

**Step:** When I send the CAN packet '{packet}' from the DUT using '{manager}'
------------------------------------------------------------------------------

Sends a packet *from* the can0 interface of the DUT, using
the cansend utility. For the step to work, a connected shell manager
(serial console or ssh for instance) must be available, and the can-utils
package must be installed on the DUT.

:param packet: A CAN packet given in a valid format for cansend (see the
    cansend's man page for more informations)
:param manager: The connected shell manager (serial ou ssh for instance)

.. index:: 
    single: Then step; Then I should receive '{packet}' on '{name}'

.. _then i should receive '{packet}' on '{name}':

**Step:** Then I should receive '{packet}' on '{name}'
------------------------------------------------------

Step that expect a can packet on a can interface.

:param name: CAN manager name to expect the packet on.
:param packet: A CAN packet to expect.

    It must be given either as the payload only, as
    an hexadecimal string of at most 16 characters, or
    with the form: 'ID#DATA', with ID and DATA hexadecimal
    strings.

.. index:: 
    single: When step; When I listen for CAN packets on the DUT using '{manager}'

.. _when i listen for can packets on the dut using '{manager}':

**Step:** When I listen for CAN packets on the DUT using '{manager}'
--------------------------------------------------------------------

Begins to dump the data received on the CAN bus of the DUT,
using candump and a connected shell (serial or ssh for instance).

It requires the can-utils package to be installed on the DUT to work

:param manager: Manager handling the connected shell.

.. index:: 
    single: Then step; Then I should receive '{string}' on the DUT using '{manager}'

.. _then i should receive '{string}' on the dut using '{manager}':

**Step:** Then I should receive '{string}' on the DUT using '{manager}'
-----------------------------------------------------------------------

Expects a CAN packet received by the DUT.

It assumes that the DUT is currently listening for CAN packets,
and printing them on a shell. It requires a connected shell.

:param string: Representation of the expected output from the
    candump tool.
:param manager: Manager handling the connected shell (serial or
    ssh)

