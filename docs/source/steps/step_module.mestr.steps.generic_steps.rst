.. _docid.steps.mestr.steps.generic_steps:
.. index:: mestr.steps.generic_steps

======================================================================
mestr.steps.generic_steps
======================================================================

:Module:   mestr.steps.generic_steps
:Filename: mestr/steps/generic_steps.py

Some generic steps that can be used with several Managers.

Step Overview
=============


=============================================================================== ===== ==== ==== ====
Step Definition                                                                 Given When Then Step
=============================================================================== ===== ==== ==== ====
Given '{manager}' has to be '{status}'                                            x                 
Given '{manager}' has to be in '{status}' state                                   x                 
When I set '{manager}' '{status}'                                                       x           
When I set '{manager}' to '{status}' state                                              x           
Given '{manager}' is '{status}'                                                   x                 
Then '{manager}' should be '{status}'                                                        x      
Then '{manager}' should be in '{status}' state                                               x      
When I wait '{delay:g}' seconds                                                         x           
Then I wait '{delay:g}' seconds                                                              x      
When '{manager}' sends '{data}'                                                         x           
When '{manager}' receives '{data}' in the '{timeout:d}' following seconds               x           
When '{manager}' receives '{data}'                                                      x           
Then '{manager}' should receive '{data}' in the '{timeout:d}' following seconds              x      
Then '{manager}' should receive '{data}'                                                     x      
Given '{manager}' has to be working                                               x                 
=============================================================================== ===== ==== ==== ====

Step Definitions
================

.. index:: 
    single: Given step; Given '{manager}' has to be '{status}'

.. _given '{manager}' has to be '{status}':

**Step:** Given '{manager}' has to be '{status}'
------------------------------------------------

Test the state of the manager. If the state is not the expected one,
set it.

:param manager: manager to check/set the status
:param status: status to check/set

.. index:: 
    single: Given step; Given '{manager}' has to be in '{status}' state

.. _given '{manager}' has to be in '{status}' state:

**Step:** Given '{manager}' has to be in '{status}' state
---------------------------------------------------------

Test the state of the manager. If the state is not the expected one,
set it.

:param manager: manager to check/set the status
:param status: status to check/set

.. index:: 
    single: When step; When I set '{manager}' '{status}'

.. _when i set '{manager}' '{status}':

**Step:** When I set '{manager}' '{status}'
-------------------------------------------

Test the state of the manager. If the state is not the expected one,
set it.

:param manager: manager to check/set the status
:param status: status to check/set

.. index:: 
    single: When step; When I set '{manager}' to '{status}' state

.. _when i set '{manager}' to '{status}' state:

**Step:** When I set '{manager}' to '{status}' state
----------------------------------------------------

Test the state of the manager. If the state is not the expected one,
set it.

:param manager: manager to check/set the status
:param status: status to check/set

.. index:: 
    single: Given step; Given '{manager}' is '{status}'

.. _given '{manager}' is '{status}':

**Step:** Given '{manager}' is '{status}'
-----------------------------------------

Given('I am "{status}" from "{manager}"')
Checks if a Manager is in a particular status.

:param manager: manager name
:param status: status to test

.. index:: 
    single: Then step; Then '{manager}' should be '{status}'

.. _then '{manager}' should be '{status}':

**Step:** Then '{manager}' should be '{status}'
-----------------------------------------------

Given('I am "{status}" from "{manager}"')
Checks if a Manager is in a particular status.

:param manager: manager name
:param status: status to test

.. index:: 
    single: Then step; Then '{manager}' should be in '{status}' state

.. _then '{manager}' should be in '{status}' state:

**Step:** Then '{manager}' should be in '{status}' state
--------------------------------------------------------

Given('I am "{status}" from "{manager}"')
Checks if a Manager is in a particular status.

:param manager: manager name
:param status: status to test

.. index:: 
    single: When step; When I wait '{delay:g}' seconds

.. _when i wait '{delay:g}' seconds:

**Step:** When I wait '{delay:g}' seconds
-----------------------------------------

Sleeps for a while

:param delay: time in seconds to wait

.. index:: 
    single: Then step; Then I wait '{delay:g}' seconds

.. _then i wait '{delay:g}' seconds:

**Step:** Then I wait '{delay:g}' seconds
-----------------------------------------

Sleeps for a while

:param delay: time in seconds to wait

.. index:: 
    single: When step; When '{manager}' sends '{data}'

.. _when '{manager}' sends '{data}':

**Step:** When '{manager}' sends '{data}'
-----------------------------------------

Send data through a manager.
Can be used with any Manager providing the "send" method.

:param data: data to send
:param manager: manager name

.. index:: 
    single: When step; When '{manager}' receives '{data}' in the '{timeout:d}' following seconds

.. _when '{manager}' receives '{data}' in the '{timeout:d}' following seconds:

**Step:** When '{manager}' receives '{data}' in the '{timeout:d}' following seconds
-----------------------------------------------------------------------------------

Expect some data from a manager.
Can be used with any Manager providing the "expect" method

:param data: data expected
:param manager: manager name
:param timeout: timeout to expect data

.. index:: 
    single: When step; When '{manager}' receives '{data}'

.. _when '{manager}' receives '{data}':

**Step:** When '{manager}' receives '{data}'
--------------------------------------------

Expect some data from a manager.
Can be used with any Manager providing the "expect" method

:param data: data expected
:param manager: manager name
:param timeout: timeout to expect data

.. index:: 
    single: Then step; Then '{manager}' should receive '{data}' in the '{timeout:d}' following seconds

.. _then '{manager}' should receive '{data}' in the '{timeout:d}' following seconds:

**Step:** Then '{manager}' should receive '{data}' in the '{timeout:d}' following seconds
-----------------------------------------------------------------------------------------

Expect some data from a manager.
Can be used with any Manager providing the "expect" method

:param data: data expected
:param manager: manager name
:param timeout: timeout to expect data

.. index:: 
    single: Then step; Then '{manager}' should receive '{data}'

.. _then '{manager}' should receive '{data}':

**Step:** Then '{manager}' should receive '{data}'
--------------------------------------------------

Expect some data from a manager.
Can be used with any Manager providing the "expect" method

:param data: data expected
:param manager: manager name
:param timeout: timeout to expect data

.. index:: 
    single: Given step; Given '{manager}' has to be working

.. _given '{manager}' has to be working:

**Step:** Given '{manager}' has to be working
---------------------------------------------

This step is working according to integrity tests it will check
if the manager is working according to integrity test result.
Considere manager as working if the status is not failed.
(should only be used for integrity tests)

:param manager: Manager that should be working

