.. _docid.steps.mestr.steps.dhcp_steps:
.. index:: mestr.steps.dhcp_steps

======================================================================
mestr.steps.dhcp_steps
======================================================================

:Module:   mestr.steps.dhcp_steps
:Filename: mestr/steps/dhcp_steps.py

Steps linked to network and dhcp related tests

Step Overview
=============


====================================================================================== ===== ==== ==== ====
Step Definition                                                                        Given When Then Step
====================================================================================== ===== ==== ==== ====
When the User disconnects and reconnects the cable from '{name}' during '{duration:d}'         x           
When the User disconnects and reconnects the cable from '{name}'                               x           
Then a DHCP operation should happen on '{name}' within '{timeout:d}' seconds                        x      
====================================================================================== ===== ==== ==== ====

Step Definitions
================

.. index:: 
    single: When step; When the User disconnects and reconnects the cable from '{name}' during '{duration:d}'

.. _when the user disconnects and reconnects the cable from '{name}' during '{duration:d}':

**Step:** When the User disconnects and reconnects the cable from '{name}' during '{duration:d}'
------------------------------------------------------------------------------------------------

This step should simulate a network cable short disctonnection.

.. warning:: For now, it does not work (see documentation -> Area for
    improvements for more informations)

:param name: The name of the interface which should be disconnected.
:param duration: Duration of the disconnection, in seconds (default: 20s)

.. index:: 
    single: When step; When the User disconnects and reconnects the cable from '{name}'

.. _when the user disconnects and reconnects the cable from '{name}':

**Step:** When the User disconnects and reconnects the cable from '{name}'
--------------------------------------------------------------------------

This step should simulate a network cable short disctonnection.

.. warning:: For now, it does not work (see documentation -> Area for
    improvements for more informations)

:param name: The name of the interface which should be disconnected.
:param duration: Duration of the disconnection, in seconds (default: 20s)

.. index:: 
    single: Then step; Then a DHCP operation should happen on '{name}' within '{timeout:d}' seconds

.. _then a dhcp operation should happen on '{name}' within '{timeout:d}' seconds:

**Step:** Then a DHCP operation should happen on '{name}' within '{timeout:d}' seconds
--------------------------------------------------------------------------------------

This step captures dhcp packets on the network, and succeeds
if in the first transaction captured, a DHCP REQUEST and a DHCP
ACK are found.

.. note:: This step is meant as an exemple of what can be done with the
    NetworkManager's dhcp_sniffer. It can be improved for a better analysis
    of the captured packets (make sure that the requested IP matches the one
    stored in the NetworkManager for instance).

:param name: name of the network manager.
:param timeout: Duration of the packet capture.

