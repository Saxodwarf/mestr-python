.. _docid.steps.mestr.steps.web_steps:
.. index:: mestr.steps.web_steps

======================================================================
mestr.steps.web_steps
======================================================================

:Module:   mestr.steps.web_steps
:Filename: mestr/steps/web_steps.py

Step Overview
=============


=========================================================================== ===== ==== ==== ====
Step Definition                                                             Given When Then Step
=========================================================================== ===== ==== ==== ====
Given '{name}' is able to reach the web server                                x                 
Then '{name}' is able to reach the web server                                            x      
When I 'connect' to '{name}' with '{username}' and '{password}' credentials         x           
Then '{name}' should be able to visit '{url}'                                            x      
=========================================================================== ===== ==== ==== ====

Step Definitions
================

.. index:: 
    single: Given step; Given '{name}' is able to reach the web server

.. _given '{name}' is able to reach the web server:

**Step:** Given '{name}' is able to reach the web server
--------------------------------------------------------

send a request to the **url** set on the webManager and expect a 200 status

:param name: web_api manager name

.. index:: 
    single: Then step; Then '{name}' is able to reach the web server

.. _then '{name}' is able to reach the web server:

**Step:** Then '{name}' is able to reach the web server
-------------------------------------------------------

send a request to the **url** set on the webManager and expect a 200 status

:param name: web_api manager name

.. index:: 
    single: When step; When I 'connect' to '{name}' with '{username}' and '{password}' credentials

.. _when i 'connect' to '{name}' with '{username}' and '{password}' credentials:

**Step:** When I 'connect' to '{name}' with '{username}' and '{password}' credentials
-------------------------------------------------------------------------------------

When("I 'connect' to '{name}' with '{username}' and '{password}'    credentials")
   method to connect to websocket or web_api manager using specific
   username and password

:param name: websocket or web_api manager name
:param username: username
:param password: password

.. index:: 
    single: Then step; Then '{name}' should be able to visit '{url}'

.. _then '{name}' should be able to visit '{url}':

**Step:** Then '{name}' should be able to visit '{url}'
-------------------------------------------------------

Then("'{name}' should be able to visit '{url}'")
try to reach an url with web_api. Test the return  code and
if the url is the same as the one requested

:param name: web_api manager name
:param url: expected url

