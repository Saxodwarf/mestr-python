.. _docid.steps.mestr.steps.websocket_steps:
.. index:: mestr.steps.websocket_steps

======================================================================
mestr.steps.websocket_steps
======================================================================

:Module:   mestr.steps.websocket_steps
:Filename: mestr/steps/websocket_steps.py

Step Overview
=============


===================================================== ===== ==== ==== ====
Step Definition                                       Given When Then Step
===================================================== ===== ==== ==== ====
When I 'connect' to '{manager}' using '{api}' manager         x           
===================================================== ===== ==== ==== ====

Step Definitions
================

.. index:: 
    single: When step; When I 'connect' to '{manager}' using '{api}' manager

.. _when i 'connect' to '{manager}' using '{api}' manager:

**Step:** When I 'connect' to '{manager}' using '{api}' manager
---------------------------------------------------------------

When('I "{action}" to/from "{manager}" using "{api}" manager')
   method to connect to websocket manager using api authentication cookies

:param manager: websocket manager name
:param api: web_api manager name

