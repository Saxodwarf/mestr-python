.. _portability-ref:

Using Mestr on an other platform
========================================

You may want to use MESTR on a platform which is not the Beaglebone Black, but which contains interfaces that fit your needs better.

Even if we tried as much as possible to make MESTR's code portable, it was not possible to do it for all the features, especially for the ones linked to pin multiplexing. Therefore some part of the code are using libraries that are specific to the Beaglebone black.

This page aims to document the pieces of code that must be adapted when changing the base platform, and how to change them.

Managers
-------------

The ``GpioManager``
^^^^^^^^^^^^^^^^^^^^^^^^

This manager uses the AdafruitBBIO library to handle the GPIO pins.
It will need to be rewriten using an other library to work on an other device.
This is quite a bit of work, but it is inevitable...

It also lists all the available pins of the board by looking in ``/sys/class/gpio/``. This might need some adjustments.

The ``SerialManager``
^^^^^^^^^^^^^^^^^^^^^^^^

Here, the dependancy is way less important. However, the AdafruitBBIO library is also used, to put the desired pins in uart mode.

The only call to the library is located in the ``init_pins()`` function, in the serial_man.py file.

You may want to redefine this function, to initialise the pins your own way, or to not initialize the pins at all, if they are already initialized somewhere else.

The ``CanManager``
^^^^^^^^^^^^^^^^^^^^^^^^

As for the ``SerialManager``, this manager uses a Beaglebone-specific call to put the pins in CAN mode.

It is located in the ``init_can()`` function of the can_man.py file, and must be updated in case of a platform change, to configure your CAN interface your own way, or to leave it to an other part of your setup.

The ``RelayManager``
^^^^^^^^^^^^^^^^^^^^^^^^

As the ``RelayManager`` uses a ``GpioManager`` internally, adapting the ``GpioManager`` while keeping the same interface should be enough to make the RelayManager work on the new platform.


Scripts
-------------

Some of the configuration scripts may not work on your new platform, because of either missing packages, or paths that are not the same in your setup.

For instance, the networkConfiguration script makes some calls to the network manager of the Beaglebone, named Connman. Therefore, it will likely need some adaptations.

However, if the OS is Debian, there is no reason why the routing scripts would not work.
The same applies to the rights configuration script, but it needs a filesystem that supports extended file attributes, as it uses files capabilities which are stored as extended attributes.

.. warning:: Please check the scripts with your platform specificities in mind before running them as root !



