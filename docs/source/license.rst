License
==========

This program is placed under a double license: ISC ang GPLv3+.

You can find a copy of the license file in the `project's repository <https://gitlab.com/essensium-mind/mestr>`_.

We are using some dependancies, that are placed under the following licences:


============================================ ============================================ ============================================
Library                                                 License                                 Link to the license
============================================ ============================================ ============================================
Pexpect                                         ISC                                         `License <https://github.com/pexpect/pexpect/blob/master/LICENSE>`__
dpkt                                            BSD 3 clauses                               `License <https://github.com/kbandla/dpkt/blob/master/LICENSE>`__
Behave                                          BSD 2 clauses                               `License <https://github.com/behave/behave/blob/master/LICENSE>`__
Allure-python                                   Apache 2.0                                  `License <https://github.com/allure-framework/allure-python/blob/master/LICENSE>`__
Allure                                          Apache 2.0                                  `License <https://github.com/allure-framework/allure2/blob/master/LICENSE>`__
Ruamel-yaml                                     MIT                                       LICENSE file in the `repository <https://bitbucket.org/ruamel/yaml/src>`__ of the project
Adafruit-bbio                                   MIT                                       See the end of `this <https://github.com/adafruit/adafruit-beaglebone-io-python/blob/master/README.md>`__
pyserial                                        BSD 3 clauses                               `License <https://github.com/pyserial/pyserial/blob/master/LICENSE.txt>`__
Requests                                        Apache 2.0                                  `License <https://github.com/requests/requests/blob/master/LICENSE>`__
websocket-client                                BSD 3 clause                                `License <https://github.com/websocket-client/websocket-client/blob/master/LICENSE>`__
============================================ ============================================ ============================================
