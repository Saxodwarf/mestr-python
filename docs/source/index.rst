.. mestr documentation master file, created by
   sphinx-quickstart on Tue Nov 13 11:04:12 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mestr's documentation!
==================================================================

**Mestr** is a python framework for embedded prototype testing. It create an abstraction layer between the **device under Test (DUT)** and the testing platform.

This software is developped to be run on a **Beaglebone Black** and uses `behave <https://behave.readthedocs.io/en/latest/>`_ as test framework. It should be able to connect with any integration tool. It's currently used and tested with `gitlab-ci <https://docs.gitlab.com/ee/ci/>`_. Likewise, behave creates a report that could be processed by many tools. We are currently using the `allure framework <https://docs.qameta.io/allure/>`_

The project repository can be found `here <https://gitlab.com/essensium-mind/mestr>`_.

How does it work ?
------------------

When a test job is launched by gitlab-ci, the gitlab-runner is triggered
in the beaglebone.

Then, **Mestr** reads a yaml file containing all the informations needed
to instantiate helper classes that manage connexions between the DUT and
the platform. This yaml file is stored on the test platform, and should
not change a lot.

An other yaml file can be used to configure the test framework and
select the tests that will be run, but it is not mandatory. This
configuration file can be stored along the test cases.

After that, **Mestr** launches **behave** , and passes the helper
classes as context variable.

When **behave** stops, the reports are sent as artifacts to gitlab-ci.
These reports can then be processed by the Allure framework to create a
static web site with a nice representation of the reports.

Architecture
~~~~~~~~~~~~

.. image:: ../imgs/mestr_organisation.png
   :alt: Organisation of MESTR
   :align: center


Workflow
~~~~~~~~

.. image:: ../imgs/testExecution.png
   :alt: Workflow for MESTR
   :align: center



.. toctree::
   :maxdepth: 2
   :caption: Installation:

   boneSetup

.. toctree::
   :maxdepth: 2
   :caption: QuickStart:

   quickstart
   
.. toctree::
   :maxdepth: 1
   :caption: Advanced Usage:
    
   managers
   config_yml
   can_bus
   tags
   network
   gitlab-ci_allure_history
   other_platform
   
.. toctree::
   :maxdepth: 1
   :caption: Reference:

   mestr
   mestr.managers
   modules
   steps/index

.. toctree::
   :maxdepth: 1
   :caption: About the project:

   improvement
   license

Indices and tables
=============================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
