.. _can_bus_ref:

The CAN bus on the beaglebone
==================================

To use the beaglebone's CAN bus, a CAN transceiver must be added to the setup.
You can either buy it alone and plug it in the right place, or use a cape with
a built in CAN transceiver.

We are using the official `Communications Cape <https://beagleboard.org/capes>`__
from beagleboard.org. Aside from a CAN transceiver, it also provides some other
interesting interfaces. However, this cape doesn't have a lot of documentation
(if any documentation at all), and the cape's device tree overlay is missing...

Nevertheless, we managed to use the CAN transceiver from the cape thanks to Cape
Universal and its ``config-pin`` command. An other possibility would have been
to use the BB-CAN1-00A0 overlay, which would have put the right pins in can mode
as well, but which would have been a less flexible solution.

The CAN transceiver of the Comm cape is plugged into the pin ``P9_24`` and ``P9_26``
of the beaglebone, so those two pins must be put into "can" mode, like so :

.. code-block:: bash

    config-pin P9.24 can
    config-pin P9.26 can

Then, the can bus must be enabled, using the ``ip`` command :

.. code-block:: bash

    sudo ip link set can1 up type can bitrate 125000

The changes made above are not reboot-persistent. To make them reboot-persistent,
the ``/etc/interfaces`` file must be edited to set up the can1 interface on boot,
and the pins must be configured automatically, either by using the CAN overlay,
or by creating a script, running on startup, that would execute the two needed
``config-pin`` commands.

Then, the CAN bus can be used in python using the socket mechanism.

In our :class:`CanManager <mestr.managers.can_man.CanManager>` class, we configure automatically the needed pins, so all you have to do is configuring the CAN network interface.
