Write your ``managers.yml`` file
--------------------------------------------------

To define your connections between the **Mestr**'s setup and the **DUT**, you need a configuration file.
This file contains the different managers which are connected to the **DUT** and should be saved as ``/home/gitlab-runner/managers.yml`` by default. For more details, see :ref:`managers.yml<managers-yml-ref>`. You can also  define specific running options in a :ref:`config.yml<config-yml-ref>` file.

.. code-block:: yaml

    # each manager must have at least :
    # - type : use to find the related class. Type  is defined by the method mod_type
    # - name : name to refer to it in the feature files
    # manager can also have other arguments which will be pass to the __init__ method
    
    Managers : 
        - # manager for serial connection
            type: serial  
            name : debug 
            target : 2
            baudrate : 115200

        - # relay manager to power up the DUT
            type : relay 
            name : power  
            gpio : P9_15
            mode: ACT_LOW
            
Write your tests (features) files
---------------------------------------------

**Mestr** uses behave as test framework. 

    "Behave is behaviour-driven development, Python style. Behavior-driven development (or BDD) is an agile software development technique that encourages collaboration between developers, QA and non-technical or business participants in a software project. " `link <https://behave.readthedocs.io/en/latest/index.html>`_
    
Behave uses tests written in a natural language style, backed up by Python code. Tests are so written in `gherkin <https://behave.readthedocs.io/en/latest/gherkin.html#gherkin-feature-testing-language>`_ language.

Write a test : 

    * Create your feature and scenarii
    * Use already defined steps with manager name defined in your ``managers.yml`` file (:ref:`list here <docid.steps>`)
    * Add manager tags with format : ``manager.<manager_name>`` to specify wich manager is used  (:ref:`details <tags-ref>`)

If **debug** and **relay** are defined in the ``managers.yml`` file, you can write the following tests based on already defined steps. Save the file created in the directory ``tests/features`` (default one): 

.. code-block:: gherkin
    
    # Tag list to run only test with available manager
    @manager.power
    @manager.debug
    Feature: testing boot process
        this feature test the boot process. It first try a boot scenario without 
        trouble and secondly a boot scnenario with a power cut during boot
        process

    Scenario: boot integrity
        Given 'power' has to be 'off'
        When I set 'power' 'on'
        Then 'debug' should receive 'Dobiss system' in the '120' following seconds
    
    Scenario Outline: boot with trouble
        Given 'power' has to be 'off'
        When I set 'power' 'on'
        And I wait '10' seconds
        And I set 'power' 'off'
        And I wait '1' seconds
        And I set 'power' 'on'
        Then 'debug' should receive '<login>' in the '120' following seconds
        # <login> should be a pattern you can match when the device is booted
        
Create a default steps and environement files
---------------------------------------------------

To have access to the already defined steps, you have to create a **step** directory in the features one containing at least one python file importing the predefined steps.

.. code-block:: python

    # import mestr steps
    from mestr.steps import *

Besides, Mestr uses behave environment function for custom operations. To enable these operations, create a file ``environment.py`` in the features directory with this content : 

.. code-block:: python
    
    from mestr.utils.base_env import (mestr_before_all, mestr_after_scenario,
                                  mestr_before_feature, mestr_before_scenario)

    @mestr_before_all
    def before_all(context: Context)->None:
        """function executed before all tests are run"""
        pass


    @mestr_before_feature
    def before_feature(context: Context, feature: Feature) -> None:
        """function executed before all feature"""
        pass


    @mestr_before_scenario
    def before_scenario(context: Context, scenario: Scenario) -> None:
        """function executed before each scenario"""
        pass


    @mestr_after_scenario
    def after_scenario(context, scenario):
        """function executed after each scenario"""
        pass
    
Run **Mestr** 
-----------------------------

To run **Mestr**, you first have to initialize `pipenv` : ``pipenv install``.
If all files are in default location, you can then run **Mestr**: ::

    pipenv run python __main__.py


An other option is to use the gitlab-ci jobs. You also have to initialize pipenv by running the job `setup`.

You can then run the job `mestr`.

An example of a basic ``.gitlab-ci.yml``, ``environment.py`` and ``steps.py`` can be found in the ``docs/examples`` directory.

