""" fonctions for parsing yaml config file and managers directory"""
import os
from typing import Dict, List, Any, Union
from ruamel.yaml import YAML
from .managers.manager import Manager
# pylint: disable=wildcard-import
from .managers import *
from .utils.types_aliases import StrDict

CONFIG_PATH = "prototype_tests/config.yml"
MAN_PATH = "/home/gitlab-runner/managers.yml"


def list_managers()->Dict[str, Manager]:
    r"""
    list subclasses of class manager and return a dictionary with result like :
        "man_type" : *subclass
    """
    subclasses = list_all_subclasses(Manager)
    return {cls.man_type(): cls for cls in subclasses}


def list_all_subclasses(cls: Any)->List[Any]:
    """ recursive function to list all subclasses from a root one"""
    all_subclasses = list()
    for sub_class in cls.__subclasses__():
        all_subclasses.append(sub_class)
        all_subclasses.extend(list_all_subclasses(sub_class))
    return all_subclasses


# return structure format of yaml_config function
DataFormat = Dict[str, Union[bool, str, Dict[str, Manager], List[List[str]]]]


def yaml_config(url: str, man_path: str)->DataFormat:
    """
    parse configuration file and return informations in a structure like :

    .. code-block:: yaml

    # Dictionary of manager name and ther implementations
    managers :
        name : object
        name : object...
    # behave formated str to call specific formater and output file/directory
    Reports : ' -f reporter -o location'
    # Directory to save debug files (same as location or directory containing
    #the location dile)
    Log_files : 'location'
    # List of files/directories for features locations
    Features :  'features location'
    # list of lists of tags coressponding to (@foo OR @bar) AND @zap
    Tags : [["foo", "bar"], ["zap"]]
    """
    url = url or CONFIG_PATH
    man_path = man_path or MAN_PATH
    data: DataFormat = dict()
    yaml = YAML(typ='safe')
    if os.path.isfile(url):
        # parse config.yml arguments
        with open(url) as file:
            yaml_data = yaml.load(file)
        features = ""

        if "Features" in yaml_data:
            features = yaml_data["Features"]

        if "Reports" in yaml_data:
            data.update(_parse_reports(yaml_data["Reports"]))

        if "Stage" in yaml_data:
            data["Stage"] = yaml_data["Stage"]

        if "Tags" in yaml_data:
            data["Tags"] = _parse_tags(yaml_data["Tags"])

        if "Filter" in yaml_data:
            data["Filter"] = yaml_data["Filter"]

        data["Features"] = features

        # let priority to command line argument against specified by file
        if man_path == MAN_PATH and "Managers" in yaml_data:
            man_path = yaml_data["Managers"]

    # manager management
    if os.path.isfile(man_path):
        with open(man_path) as man_file:
            man_data = yaml.load(man_file)
    else:
        raise FileNotFoundError("manager file {} doesn't exist"
                                .format(man_path))

    data["managers"] = _parse_man(man_data["Managers"])
    return data


def _parse_reports(rep_section: StrDict)->StrDict:
    """parse report section of config.yml
    For an input like this :

    .. code-bloks:: yaml

              output: ./prototype_tests/reports/
              formatter:  allure_behave.formatter:AllureFormatter

    The result should be :

    .. code-blocks::yaml

            {"Reports" : "allure_behave.formatter:AllureFormatter",
                "Output" : "./prototype_tests/reports/",
                "Log_files" : "./prototype_tests/reports/"}

    :param rep_section: yaml parsed report section

    """
    # argument to pass to behave command line
    formatter = ""
    output = ""
    # directory to save manager log
    log_file_path = ""

    if "formatter" in rep_section:
        formatter = rep_section["formatter"]

    if "output" in rep_section:
        out = rep_section["output"]
        if os.path.isdir(out):
            # with default  formatter, behave doesn't accept a directory
            # as output
            if "formatter" in rep_section:
                output = out
            log_file_path = out
            if log_file_path[-1] != "/":
                log_file_path += "/"
        else:
            # output is a file or formatter may accept a directory
            if out[-1] != "/" or "formatter" in rep_section:
                output = out
            if out:
                log_file_path = os.path.dirname(out) + "/"
    return {"Formatter": formatter, "Log_files": log_file_path, "Output": output}


def _parse_tags(tags_section: List[str])->List[List[str]]:
    """parse tags section of config.yml

     :param tags_section: yaml parsed tags section
     :return: Structure like [["foo", "bar"], ["zap"]] equivalent to (@foo OR
                    @bar) AND @zap

    """
    tag_lists: List[List[str]] = list()
    for tag_line in tags_section:
        # put tag in the same line in a list
        tag_list = tag_line.replace(" ", "").split(",")
        # create a list of list for tags in differents lines
        tag_lists.append(tag_list)
    return tag_lists


def _parse_man(man_section: List[Dict[str, str]])->Dict[str, Manager]:
    """parse Managers section of config.yml

    For each manager, function look at :

     * **type** key : Then try to match it with the list of available manager
     * **name** key :

    :param man_section: yaml parsed managers section
    :return: dictionary of managers with name as key
    """
    managers: Dict[str, Manager] = dict()
    mod_available = list_managers()

    for man in man_section:
        man_type = man["type"]
        name = man["name"]
        del man["type"]
        del man["name"]
        # creates manager from configuration. Store it with <name> key
        managers[name] = mod_available[man_type](**man)
    return managers
