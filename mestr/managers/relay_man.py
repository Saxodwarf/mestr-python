"""manager in charge of managing a relay"""
import time
from typing import Union
from .manager import Manager
from .gpio_man import GpioManager


class RelayManager(Manager):
    """Class that represent a relay managed by a GPIO port.

    The manager can be defined as :

    .. code-block:: yaml

        -
            type : relay
            name : yourName
            gpio : P9_15
            mode : ACT_LOW # optionnal
            value : False # optionnal

    :param pin: pin for commanding the relay
    :param mode: normally open (ACT_LOW / 0) OR normally close
        (ACT_HIGH / 1) [default=ACT_LOW]
    :param value: initial value of the relay [default=False]
    :param delay: delay (in s) to wait after changing the state of the relay to
        let the hardware time to change. (default: 0.5s)
    """
    ACT_LOW = 0
    ACT_HIGH = 1
    HIGH = True
    LOW = False

    def __init__(self, gpio: str, mode: Union[int, str] = ACT_LOW,
                 value: int = 0, delay=0.5):
        self.gpio = GpioManager(gpio=gpio, direction=GpioManager.OUT,
                                value=value, mode=mode)
        self._available_states = {
            "on": lambda self: RelayManager.set_value(self, 1),
            "off": lambda self: RelayManager.set_value(self, 0)
            }
        self.delay = delay

    @staticmethod
    def man_type()->str:
        "relay"
        return "relay"

    @property
    def pin(self)->str:
        """Accessor for the gpio pin value."""
        return self.gpio.pin

    def is_state(self, state: str)->bool:
        state_translation = {"on": "high",
                             "off": "low"}
        if state in state_translation:
            return self.gpio.is_state(state_translation[state])
        return super().is_state(state)

    def set_value(self, state: int) -> None:
        """Set the state of the relay according to the mode

        :param state: 1/True means close the circuit and 0/False open it
        """
        self.log("Setting relay of pin "
                 "{} state to {}\n".format(self.gpio.pin, state))
        self.gpio.set_value(state)
        time.sleep(self.delay)
