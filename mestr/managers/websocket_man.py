"""Module to manage a websocket connection"""
import re
import signal
import select
from typing import List, NoReturn, Optional, Union
from urllib.parse import SplitResult
from requests.auth import AuthBase
import websocket
from mestr.utils.exceptions import MestrTimeout
from .manager import Manager
from ..utils import web
from .web_man import WebRequest
from ..utils.types_aliases import Expect, StrDict


def dict_to_cookie(dct: StrDict)-> str:
    """
    Transforms a dictionary to a string cookie with format :
    'cookie1=value ; cookie2=value...'
    """
    if dct:
        output = list()
        for item in dct.items():
            output.append(item[0] + "=" + item[1])
        return " ; ".join(output)
    return ""


def time_over(signum, frame)->NoReturn:
    """
    Raises a :class:`MestrTimeout <mestr.utils.exceptions.MestrTimeout>`
    exception
    """
    raise MestrTimeout("time is over")


class WebSocketManager(Manager):
    """
    This class is setting up websocket connection. It use the websocket-client
    library to handle websocket protocol.

    The manager can be defined as :

    .. code-block:: yaml

        -
            type : websocket
            name : yourName
            url: None # optionnal
            auth : None # optionnal
            url_login : None # optionnal
            headers : None # optionnal
            cookies : None # optionnal
            timeout : None # # optionnal

    parameters :

    :param url: url used for socket creation. If no protocol is set, it assume
        it's 'ws' protocol (url)
    :param auth: connection is managed by the WebRequest class itself based
        on requests library. So auth can be the same as the requests one
        (tuple with (login, password) for basic authentication or class
        based on AuthBase)
    :param url_login: url for authentication. If no scheme present, it
        assumes scheme is 'http'. If no netloc is present, it assumes it's
        the same as url location. (str)
    :param headers: extra headers for websocket creation (dict)
    :param cookies: extra cookies for websocket creation (dict)
    :param timeout: global timeout for expecting result from websocket

    This class tries to have a behavior close to pexpect. To use it do::

       # create to websocket
        ws_man = WebsocketManager(url, auth, url_login, headers)
        # connect to websocket
        ws_man.login()
        # send data
        ws_man.send(str_data)
        # expect result
        ws_man.expect(list_expecting_str_or_regex)
    """

    # pylint: disable=too-many-instance-attributes,too-many-arguments
    def __init__(self, url: str = None, auth: AuthBase = None,
                 url_login: str = None, headers: StrDict = None,
                 cookies: StrDict = None, timeout: int = None):
        self.headers: StrDict = dict()
        self.auth = auth
        self.cookies: StrDict = dict()
        self.timeout = timeout
        self._url: Optional[Union[str, SplitResult]] = url
        self._url_login: Optional[Union[str, SplitResult]] = url_login
        self.before: List[str] = list()
        self.after = str()
        self.web_sock: Optional[websocket.WebSocket] = None
        self.match: re.Match = None
        self._state = "closed"
        self._available_states = {"opened": WebSocketManager.login,
                                  "closed": WebSocketManager.logout}

        if cookies:
            self.cookies.update(cookies)
        if headers:
            self.headers.update(headers)

    @property
    def url(self) ->str:
        """ return url value"""
        if not self._url:
            raise ValueError("url value not set")
        elif isinstance(self._url, str):
            return self._url
        return self._url.geturl()

    @property
    def url_login(self)->str:
        """ return url_login value"""
        if not self._url_login:
            raise ValueError("url value not set")
        elif isinstance(self._url_login, str):
            return self._url_login
        return self._url_login.geturl()

    @staticmethod
    def man_type()->str:
        """websocket"""
        return "websocket"

    def log(self, data: str)->None:
        super().log(data+"\n")

    def _connected(self)->bool:
        if not self.web_sock:
            return False
        try:
            self.web_sock.ping()
            self.web_sock.ping()
        except (websocket.WebSocketConnectionClosedException,
                BrokenPipeError):
            return False
        return True

    def is_state(self, state: str)-> bool:
        """Checks if the websocket is in a particular state

        :param state: (str) State to check (possible values : "opened" or
            "closed")
        :return: True if the websocket is in the right state, False otherwise.
            Raises an exception if the state is unknown.
        """
        connected = self._connected()
        if state == "opened":
            return connected
        if state == "closed":
            return not connected
        # raise exception if state is unknown.
        return super().is_state(state)

    def login(self, url: str = None, auth: AuthBase = None,
              url_login: str = None, headers: StrDict = None,
              cookies: StrDict = None, timeout: int = None)->None:
        """ Initialize connection of the websocket.

        You can run it without any argument and so use arguments defined at the
        creation or overwrite any of them by specifying it again.
        """
        # create url with default ws protocol
        self._url = web.create_url(url or self.url, scheme="ws")

        self.auth = auth or self.auth
        self.headers = headers or self.headers
        self.cookies = cookies or self.cookies
        self.timeout = timeout or self.timeout

        try:
            if self.auth:
                # create url with default http protocol and same netloc as url
                self._url_login = web.create_url(url_login or self.url_login,
                                                 netloc=self._url.netloc,
                                                 scheme="http")  # type: ignore
                req = WebRequest(url_login=self.url_login, auth=self.auth,
                                 timeout=self.timeout, headers=self.headers)
                req.login()
                self.headers.update(req.session.headers)
                self.cookies.update(req.session.cookies)
        except Exception as excpt:
            raise ConnectionError from excpt

        self.web_sock = websocket.create_connection(self.url,
                                                    headers=self.headers,
                                                    cookie=dict_to_cookie(
                                                        self.cookies),
                                                    timeout=self.timeout)
        if self.web_sock.connected:
            self._state = "opened"
            self.log("websocket open")
            return
        raise ConnectionError("login to websocket failed")

    def send(self, content: str)->None:
        """ Send data to the server
        """
        self.log(content)
        self.web_sock.send(content)

    def logout(self)->None:
        """ Close the web socket
        """
        self._state = "closed"
        self.log("websocket close")
        self.web_sock.close()

    def read(self, timeout: int = None)->str:
        """ Read the next value from the websocket

        :param timeout: set specific timeout.
            It uses the object timout then 20s if not specified. Raises a
            WebSocketTimoutException if nothing is received before timeout.
        """
        self.web_sock.settimeout(timeout or self.timeout or 20)
        data = self.web_sock.recv()
        self.log(data)
        return data

    def expect(self, expect: Expect, timeout: int = None)->int:
        """ read value from the web socket until matching data or timeout

        This function is designed to be close to the pexpect function.

        However, there are still some differencies.

        In case of success, expect will return the index of element in the
        list that matched. It will save all the previous values read in before
        attribute (list) and the current one in after attribute (str).

        :param expect: a string, an regex or a list (of str and/or regex) of
            expected values.
        :param timeout: timeout for expect function. Raises a MestrTimeout
            exception
        """
        self.before = list()
        self.after = str()
        self.match = None
        lst_re = list()
        # handle all str, regex, and list as parameter
        if isinstance(expect, list):
            for element in expect:
                if isinstance(element, str):
                    lst_re.append(re.compile(element))
                else:
                    lst_re.append(element)
        elif isinstance(expect, str):
            lst_re.append(re.compile(expect))
        else:
            lst_re.append(expect)

        # Initialise expect timeout
        signal.signal(signal.SIGALRM, time_over)
        signal.alarm(timeout or self.timeout or 10)

        # Read until something match
        try:
            while True:
                msg = self.read()
                for match in lst_re:
                    self.match = match.search(msg)
                    if self.match:
                        self.after = msg
                        signal.alarm(0)
                        self.log(" pattern matched")
                        return lst_re.index(match)
                self.before.append(msg)
        except (websocket.WebSocketTimeoutException, MestrTimeout):
            signal.alarm(0)
            raise MestrTimeout

    def clear_input(self) -> None:
        """Empty the input buffer by reading all the available data.
        """
        if self.is_state("opened"):
            try:
                while select.select([self.web_sock], [], [], 0)[0]:
                    self.web_sock.recv_frame()
            except websocket.WebSocketConnectionClosedException:
                # If something went wrong, the websocket may be closed.
                pass
