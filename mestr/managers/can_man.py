"""Helper manager to use CAN bus
"""
import socket
import select
import struct
import signal
import re
from subprocess import check_call
from typing import NoReturn, List, Pattern, AnyStr, Tuple, Optional
from mestr.utils.exceptions import MestrTimeout
from mestr.utils.types_aliases import Expect
from .manager import Manager


def init_can(pins: List[str])->None:
    """Device specific pin initialisation.

    This function may be adapted to port MESTR to a platform not
    based on a Beaglebone Black.

    :param pins: List of pin names to configure in can mode.
    """
    for pin in pins:
        check_call(["config-pin", pin, "can"])


def time_over(signum, frame)-> NoReturn:
    """Raises a :class:`MestrTimeout <mestr.utils.exceptions.MestrTimeout>`
    exception"""
    raise MestrTimeout("Global timeout reached")


class CanManager(Manager):
    r"""Helper class to use the CAN bus, providing send and receive
    methods, as well as an expect method to look for a specific
    pattern in the can data.

    For now, it only supports Classical CAN (no CAN-FD).

    The manager can be defined as :

    .. code-block:: yaml

        -
            type : CAN
            name : CANName
            interface : "can1"
            timeout : 60 # optionnal
            sock_timeout : 60 # optionnal

    Examples:

    .. code-block:: python

        import can_man

        # Send some data :
        can = can_man.CanManager("can1")
        can.send("123#01ff00")

        # Receive some data :
        result = can.recv()
        # result contains a (can_id, data) tuple.

        # Expect "hello" :
        can.expect(b"hello")

        # Expect any byte between 0xaa and 0xff :
        can.expect(br"[\xaa-\xff]")

    """

    # Format used to build and unpack the can frames.
    FMT = "<IB3x8s"

    def __init__(self, interface: str, timeout=60, sock_timeout=60, flags=0):
        """Class constructor

        Opens the socket and bind it to the chosen CAN interface.

        :param interface: CAN interface to bind to.
        :param timeout: Global timeout for the expect method (default 60s)
        :param sock_timeout: Timeout for a single frame reception (default 60s)
        """
        if interface == "can1":
            init_can(["P9_24", "P9_26"])
        else:
            raise NotImplementedError(
                "The {0} interface is not yet supported".format(interface))
        self._socket = socket.socket(
            socket.PF_CAN, socket.SOCK_RAW, socket.CAN_RAW)
        self.__interface = interface
        self.timeout = timeout
        try:
            self._socket.bind((self.__interface,))
        except OSError:
            print("Could not bind to interface {}".format(self.__interface))
            raise
        self.set_sock_timeout(sock_timeout)
        self.before: List[Tuple[int, bytes]] = list()
        self.after: Tuple[int, bytes] = None
        self.match: re.Match = None
        self.default_flags = flags

    @staticmethod
    def man_type()->str:
        """CAN"""
        return "CAN"

    def set_sock_timeout(self, sock_timeout: int)->None:
        """Updates the socket timeout

        This updates the timeout of a frame reception. If the socket
        must be put in blocking mode (and never time out when waiting
        for a frame), set the timeout to None.

        :param sock_timeout: Timeout for a single frame reception
        """
        if sock_timeout > 0 or sock_timeout is None:
            self._socket.settimeout(sock_timeout)

    def send(self, packet: str, flags=0)->None:
        """Send a CAN packet over the CAN bus.

        The packet must be given under the form <*id*>#<*data*>, where both
        *id* and *data* are string-representation of hexadecimal values.
        *data* must not be longer than 8 bytes (16 hexadecimal digits).

        :param packet: a string representing the packet to send
        :param flags: Flags to set. (default 0)
        """
        pckt = packet.partition("#")
        if not pckt[0]:
            raise ValueError("Invalid format: missing CAN id")
        if not pckt[1]:
            raise ValueError("Invalid format: missing '#' separator")
        if not pckt[2]:
            raise ValueError("Invalid format: missing data")
        can_id = int(pckt[0], 16)
        data = bytes.fromhex(pckt[2])
        self._send(can_id, data, flags)

    def _send(self, can_id: int, data: bytes, flags=0)->None:
        """Sends a data on the CAN bus.
        Apply the flags to the can_id, and sends the can frame.
        Sets automatically the CAN_EFF_FLAG if the id is longer than 11
        bytes, to use the extended packet id.

        :param can_id: Packet's CAN-ID
        :param data: Data to send (must be a bytestring of less than 8 bits.)
        :param flags: Flags to set. (default 0)
        """
        len_id = len("{:b}".format(can_id))
        if len_id > 11:
            # we must be in can2.0B mode, add socket.CAN_EFF_FLAG
            # HACK : on the beaglebone, CAN_EFF_FLAG is negative, but
            # it should be positive.
            # We are thus taking the absolute value of the constant.
            flags |= abs(socket.CAN_EFF_FLAG)
            if len_id > 29:
                raise ValueError("Too long can ID")
        flags |= self.default_flags
        can_id_fl = can_id | flags
        if not isinstance(data, bytes):
            raise TypeError("Wrong data type, found type {0}, expected {1}"
                            .format(type(data), type(bytes())))
        if len(data) > 8:
            raise ValueError(
                "data is too long for a CAN frame (max 8, got {})"
                .format(len(data)))
        self.log(can_id, len(data), data, "Sending:")
        can_pkt = struct.pack(self.FMT, can_id_fl, len(data), data)
        self._socket.send(can_pkt)

    def recv(self)->Tuple[int, bytes]:
        """Receives a CAN packet, parses it and returns its ID and
        payload.

        This is a blocking method. If a socket timeout is set, it
        raises a socket.timeout exception at the end of the timeout
        if no data is received.

        If the Extended Frame Format flag (CAN_EFF_FLAG) is not set,
        and the ID is longer than 11 bits, it is truncated at 11 bits.

        :return: a tuple in the form : (can_id, data)
        """
        can_pkt = self._socket.recv(16)
        can_id, length, data = struct.unpack(self.FMT, can_pkt)
        if can_id & socket.CAN_EFF_MASK == can_id:
            # The EFF flag is not set
            if can_id > 0x7ff:
                # if the ID is longer than 11 bits
                can_id &= 0x7ff  # Keep only the 11 most rightwards bits
        else:
            # Else remove the flag.
            can_id &= socket.CAN_EFF_MASK
        self.log(can_id, length, data, "Receiving:")
        return (can_id, data[:length])

    def log(self,
            can_id: int,
            length: int,
            data: bytes,
            direction: str = "") ->None:
        """Log a packet into a log file or print it if there is no file
        to log into.

        :param can_id: CAN id to log
        :param length: Length of the data of the logged packet
        :param data: Content of the packet's data.
        :param direction: (optionnal) Indication on the direction of the\
        logged packet (Is it an sent or a received packet ?).
        """
        fmtted_data = " ".join(["{:02X}".format(x) for x in data[:length]])
        if len("{:b}".format(can_id)) > 11:  # extended ID
            fmtted_id = "{0:08X}".format(can_id)
        else:  # normal ID
            fmtted_id = "     {0:03X}".format(can_id)
        msg = "{0}  {1}  [{2}]  {3}\n".format(direction,
                                              fmtted_id,
                                              length,
                                              fmtted_data)
        super().log(msg)

    def close(self)-> None:
        """Closes the CAN socket"""
        self._socket.close()

    def expect(self,
               data: Expect,
               can_id: Optional[int] = None,
               timeout: int = None)->int:
        """Expect a value from the CAN bus.

        Reads frames from the CAN bus until data is found or a
        socket.timeout exception is raised. Such an exception can be
        raised for two reason : if recv times out (i.e. there has been
        to much time between two packets), or if the global time out is
        reached (i.e. there has been too much time since the methods
        call).

        It sets the before, after and match class attributes like so :

        * **before** contains a list of all the packets received before the\
        match or the timeout, as tuples in the form : (can_id, data).
        * **after** contains None if a timeout exception was raised, or the\
        data that matched, as a tuple.
        * **match** contains None if a timeout exception was raised, or a\
        match object.


        :param data: Data to look for. It can be a bytestring (which \
        will be compiled as a regex), a compiled regex, or a list of \
        bytestrings and regexes.
        :param timeout: Global timeout for the method.
        :return: The index of the data that matched if a list was passed, \
        and 0 otherwise.
        """
        self.before = list()
        self.after = None
        self.match = None
        is_ndef_id = can_id is None
        timeout = timeout or self.timeout
        lst_re: List[Pattern[AnyStr]] = list()
        # handle all str, regex, and list as parameter
        if isinstance(data, list):
            for elmt in data:
                if isinstance(elmt, bytes):
                    lst_re.append(re.compile(elmt))
                elif isinstance(elmt, re.Pattern):
                    lst_re.append(elmt)
                else:
                    raise TypeError("Wrong data type (must be bytes, compiled \
                        regex or list of those two types).")
        elif isinstance(data, bytes):
            lst_re.append(re.compile(data))
        elif isinstance(data, re.Pattern):
            lst_re.append(data)
        else:
            raise TypeError("Wrong data type.")
        signal.signal(signal.SIGALRM, time_over)
        signal.alarm(timeout)
        try:
            while True:
                pckt = self.recv()
                for pattern in lst_re:
                    self.match = pattern.search(pckt[1])
                    if self.match and (is_ndef_id or can_id == pckt[0]):
                        signal.alarm(0)
                        self.after = pckt
                        super().log("matched packet")
                        return lst_re.index(pattern)
                self.before.append(pckt)

        except (socket.timeout, MestrTimeout):
            self.match = None
            signal.alarm(0)
            raise MestrTimeout

    def clear_input(self) -> None:
        """Empty the input buffer by reading all the available data.
        """
        while select.select([self._socket], [], [], 0)[0]:
            self._socket.recv(16)
