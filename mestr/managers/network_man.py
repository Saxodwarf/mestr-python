""" Manager manage a network interface."""
import subprocess
from typing import List
from mestr.utils import sniffer
from mestr.utils.timestamped_file import TimestampedFile
from .manager import Manager


class NetworkManager(Manager):
    """Class to handle a network interface and the associated features.

    The manager can be defined as :

    .. code-block:: yaml

        -
            type : network
            name : network
            interface: eth0
            timeout : 30 # optionnal
            DUT_address : 192.168.0.10 # optionnal
    """
    STATE_UP = 1
    STATE_DOWN = 0

    def __init__(self, interface: str, timeout: int = 20,
                 DUT_address: str = None):
        """Class constructor

        :param interface: network interface to associate with this manager
        :param timeout: time before the Sniffer returns (default 20)
        :param DUT_address: ip address of the device under test, if known.
        """
        self.interface = interface
        self.ip_addr = DUT_address
        self.dhcp_sniffer = sniffer.DhcpSniffer(interface,
                                                timeout,
                                                log=self.log)

    @staticmethod
    def man_type()->str:
        """network"""
        return "network"

    def is_state(self, state: str)-> bool:
        """Checks if the network interface is in a particular state

        :param state: (str) State to check (possible values : "up" or "down")
        :return: True if the interface is in the right state, False otherwise.
            Raises an exception if the state is unknown.
        """
        if state in ("up", "down"):
            with open("/sys/class/net/{0}/operstate".format(self.interface),
                      "r") as state_file:
                status = state_file.readline()
            return status == "{0}\n".format(state)
        # raise exception if state is unknown.
        return super().is_state(state)

    def set_state(self, state):
        """ simple wrapper around :meth:`_set`` to stay compliant with
        the api.
        """
        if state in ["up", "down"]:
            self._set(state)
        else:
            raise ValueError("state {} not supported".format(state))

    def set_value(self, state: int)->None:
        """Sets the network interface's state

        :param state: State to set (STATE_UP or STATE_DOWN)
        """
        status = "up" if state else "down"
        self._set(status)

    def _set(self, state: str):
        self.log("Bringing {0} {1}\n".format(self.interface, state))
        subprocess.check_call(["ip", "link", "set", self.interface, state])
        self.log("{0} is {1}\n".format(self.interface, state))

    def start_logging(self, file_name: str)->None:
        """Sets a file to log into.

        Sets the logfile of the manager, and of the associated sniffer.

        :param file_name: name of the file to write into.
        """
        if self._logfile:
            raise ValueError("logfile exists already")
        self._logfile = TimestampedFile(file_name)
        self.dhcp_sniffer.set_output_file(file_name+"_DHCP.pcap")

    def stop_logging(self)->List[str]:
        """Closes the log file.

        :return: Name of the log file, and of the output file of
            the included sniffer
        """
        file_names: List[str] = list()
        if self._logfile and self.dhcp_sniffer.get_output_file():
            self._logfile.close()
            file_names.append(self._logfile.name)
            file_names.append(self.dhcp_sniffer.get_output_file())
            self._logfile = None
            self.dhcp_sniffer.set_output_file("")
            return file_names
        raise ValueError("logfile doesn't exist")
