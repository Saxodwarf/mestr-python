""" parent class of all managers"""
import abc
from typing import Dict, Callable, Union, List, Optional
from ..utils.timestamped_file import TimestampedFile


class Manager(abc.ABC):
    """ Base class that all Managers implementations inheritates from

    :attribute _state: current state of the manager
    :attribute _available_states: set of available states of the manager. Used
        by the method :meth:`is_state` to know of the parameter state is
        supported by the manager
    """
    _state: str = "no State"
    _available_states: Dict[str, Callable] = dict()
    _logfile: Optional[TimestampedFile] = None

    @staticmethod
    @abc.abstractmethod
    def man_type() -> str:
        """Returns the name of the manager.

        This name is used in the managers.yml file to set which type has to be
        created.

        example::

            # class implementation
            class FooManager(Manager):
                def man_type():
                    return "foo"

        .. code-block:: yaml

            # config.yml file :
            Manager :
                -
                    type : foo
                    name : fooName

        """
        return "Manager"

    def is_state(self, state: str) -> bool:
        """Compares the current state of the manager

        Subclasses should at least fill the :attr:`_available_states` attribut
        and keep up to date the state value in :attr:`_state`

        :param state: str of the state to compare with :attr:`_state` attribut
        :return: boolean value. True if **state** = :attr:`_state`, False
            if **state** included in :attr:`_available_states`.
            Raise ValueError if state not supported
        """
        if state == self._state:
            return True
        if state in self._available_states:
            return False
        raise ValueError("State not supported")

    def set_state(self, state: str)->None:
        """Put the manager to the specified state.
        this method will use the dictionary :attr:`_available_states` to match
        the right method to call in order to set the manager in the expected
        state.

        :param state: expected state

        """
        if state in self._available_states:
            self._available_states[state](self)
        else:
            raise ValueError("State not supported")

    def start_logging(self, file_name: str)->None:
        """Set a file to log into.

        :param file_name: name of the file to write into
        """
        if self._logfile:
            raise ValueError("logfile exists already")
        self._logfile = TimestampedFile(file_name)

    def log(self, data: str)->None:
        """Logs data into a log file or print it if there is no file to
        log into

        :param data: data to log.
        """
        if self._logfile:
            self._logfile.write(data)
            self._logfile.flush()
        else:
            print(data.rstrip("\n"))

    def stop_logging(self)->Union[str, List[str]]:
        """Closes the log file

        :return: name of the log file
        """
        file_name: Optional[str] = None
        if self._logfile:
            self._logfile.close()
            file_name = self._logfile.name
            self._logfile = None
            return file_name
        raise ValueError("logfile doesn't exist")

    def clear_input(self) -> None:
        """Can be overwritten to clear the input buffer of a manager at
        the end of the scenarii.
        """
