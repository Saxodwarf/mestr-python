"""Module allowing the config-parser to import all the managers at once,
and without "knowing" which manager is actually implemented.
If you implement a new manager, you must add the new manager's file name
to the list to allow the config-parser to instantiate it.
"""
__all__ = ["can_man",
           "gpio_man",
           "manager",
           "network_man",
           "relay_man",
           "serial_man",
           "ssh_man",
           "web_man",
           "websocket_man"]
