"""Module containing managers related to web api interactions."""
import copy
import difflib
import importlib
import json
from typing import Union
import requests
from requests.auth import AuthBase
from ..utils.exceptions import MestrTimeout
from ..utils import web
from .manager import Manager
from ..utils.types_aliases import StrDict, UrlParam, UrlData


Request = Union[requests.models.Response, requests.PreparedRequest, str]


# pylint: disable=too-many-instance-attributes
class WebRequest(Manager):
    """
    Class in charge of requesting data from a web api.
    Arguments can either be passed when the object is created or when you
    specificaly need them.

    :param url_login: url used for authentication.
    :param auth: auth class used for authentication this class heritate
        from request.authBase.

        If specific parameter have to be transferred to the current session,
        they have to be stored in a header dict at **self.authentication**.

        **self.authentication** can contain 2 fields :

            * **headers** which should be a list of tuple of key values.
            * **cookies** which should be a dictionnary

        They are respectively set to cookies and headers of current session

    :param headers: extra headers to be send.
    :param timeout: timeout of the request.
    :param cookies: cookie to atach.
    :param domaine: default domaine for requests
    :param protocol: default protocol for requests
    :param clean_log_req: function to clean a request from useless or \
    sensitiv data

    The manager can be defined as :

    .. code-block:: yaml

        -
            type : webrequest
            name : yourName
            auth : None # optionnal
            url_login : None # optionnal
            headers : None # optionnal
            timeout : None # optionnal
            cookies : None # optionnal

    """

    # pylint: disable=too-many-arguments
    def __init__(self, auth: Union[AuthBase, str] = None,
                 url_login: str = None,
                 headers: StrDict = None, timeout: int = None, protocol="http",
                 cookies: StrDict = None, domain: str = None,
                 clean_log_req: str = None):
        self._state = "logged out"
        self._available_states = {"logged in": WebRequest.login,
                                  "logged out": WebRequest.logout}
        self.headers = headers
        self.session = requests.Session()
        self.timeout = timeout
        self.cookies = cookies
        self._base_scheme = protocol
        self._base_netloc = domain
        self._clean_log_req = clean_log_req

        if auth:
            if isinstance(auth, str):
                self.auth = web.parse_class(auth)
            else:
                self.auth = auth

        if url_login:
            self._url_login = web.create_url(url_login,
                                             scheme=self._base_scheme,
                                             netloc=self._base_netloc)

    @property
    def url_login(self)->str:
        """Get _url_login string"""
        return self._url_login.geturl()

    @property
    def url(self)->str:
        """Get _url_login string"""
        return self._url_login.geturl()

    @staticmethod
    def man_type()->str:
        """webrequest"""
        return "WebRequest"

    def update_base_url(self, domain: str = None, protocol: str = None)->None:
        """Updates the default protocol and/or domain for requests.

        :param domaine: domaine to update
        :param protocol: protocol to update
        """
        if domain:
            self._base_netloc = domain
        if protocol:
            self._base_scheme = protocol

    def log(self, data: Union[str, Request])->None:
        """Formats requests.models.Response and PreparedRequest to display it
        in the logs

        :param data: prepared request or response

        A prepared request will be formatted like : ::

            GET  https://example.com/data?param1=foo&param2=bar
            headers :
            {
                headers...
            }
            body :
            ...body...

        A Response will be formatted like : ::

            200     https://example.com/data?param1=foo&param2=bar
            headers :
            {
                headers...
            }
            cookies :
            {
                cookies...
            }
        """
        if isinstance(data, str):
            super().log(data+"\n")
            return
        if self._clean_log_req:
            func = self._clean_log_req.split(".")
            clean_func = func.pop()
            lib = importlib.import_module(".".join(func))
            data = lib.__getattribute__(clean_func)(copy.deepcopy(data))

        log_data = list()
        if data.url:
            log_data.append("\t" + data.url + "\n")
        log_data.append(" headers :\n" +
                        json.dumps(dict(data.headers), indent=4))
        if isinstance(data, requests.models.Response):
            log_data.append("\n cookies : \n" +
                            json.dumps(dict(data.cookies), indent=4))
            log_data.insert(0, str(data.status_code))
        elif isinstance(data, requests.PreparedRequest):
            if isinstance(data.body, str):
                log_data.append("\n body : \n" + data.body)
            log_data.insert(0, data.method)  # type: ignore
        super().log("".join(filter(None, log_data)) + "\n")

    # pylint: disable=too-many-arguments
    def login(self, url: str = None, auth: AuthBase = None,
              headers: StrDict = None,
              cookies: StrDict = None,
              params: UrlParam = None,
              method="POST",
              timeout: int = None,)->requests.models.Response:
        """ Connects to the service and store auth cookie if needed

        :param url: url used for authentication. [Default=self.url_login]
        :param auth: auth class used for authentication. This class heritates
            from request.authBase.

            If specific parameter have to be transferred to the current
            session, they have to be stored in a header dict at
            **self.authentication**.

            **self.authentication** can contain 2 fields :

            * **headers** which should be a list of tuple of key values.
            * **cookies** which should be a dictionnary

            They are respectively set to cookies and headers of current session
            [Default=self.auth]

        :param headers: extra headers to be send.WARNING, the headers will
            override the first configuration. [Default=self.headers]
        :param timeout: timeout of the request. [Default=self.timeout]
        :param method: http method to use [Default="POST"]
        """
        self._url_login = web.create_url(url or self.url_login,
                                         scheme=self._base_scheme,
                                         netloc=self._base_netloc)
        auth = auth or self.auth
        headers = headers or self.headers
        timeout = timeout or self.timeout
        cookies = cookies or self.cookies
        req = requests.Request(method, self.url_login, auth=auth,
                               headers=headers, params=params)
        prep = self.session.prepare_request(req)
        self.log(prep)
        resp = self.session.send(prep, timeout=timeout)
        if resp.status_code >= 400:
            raise requests.HTTPError(
                "Connection failed : status {}".format(resp.status_code))
        if auth.authentication:
            headers = dict()
            cookies = auth.authentication.get("cookies", None)
            if cookies:
                self.session.cookies.update(cookies)
            if auth.authentication.get("headers", None):
                for header in auth.authentication["headers"]:
                    headers[header[0]] = header[1]
                self.session.headers.update(headers)
        self.log(resp)
        self._state = "logged in"
        return resp

    # pylint: disable=too-many-arguments
    def send(self, url: str, method="GET",
             headers: StrDict = None,
             data: UrlData = None,
             params: UrlParam = None,
             timeout: int = None)->requests.models.Response:
        """
        Send request and return the result

        :param url: url used for request.
        :param method: method for the resquest [Default=GET]
        :param headers: extra headers to be sent. WARNING, the headers will
            override the first configuration . [Default=self.headers]
        :param data: extra data to send with the request
        :param timeout: timeout of the request. [Default=self.timeout]
        """
        headers = headers or self.headers
        timeout = timeout or self.timeout
        if headers:
            self.session.headers.update(headers)
        url = web.create_url(url, scheme=self._base_scheme,
                             netloc=self._base_netloc).geturl()
        req = requests.Request(method, url, data=data, params=params)
        prep = self.session.prepare_request(req)
        self.log(prep)
        try:
            resp = self.session.send(prep, timeout=timeout)
        except requests.exceptions.ConnectTimeout:
            raise MestrTimeout
        self.log(resp)
        return resp

    def logout(self)->None:
        """Close the current session.

        Remove all cookies and headers relative to the session.
        Does not remove initial parameters
        """
        self.log("closing the session")
        self._state = "logged out"
        self.session.close()


class WebManager(WebRequest):
    """ Subclass of webrequest, this class only implements some easy match
    functions for testing response value

    The manager can be defined as :

    .. code-block:: yaml

        -
            type : web_api
            name : yourName
            auth : None # optionnal
            url_login : None # optionnal
            headers : None # optionnal
            timeout : None # optionnal
            cookies : None # optionnal
    """

    @staticmethod
    def man_type()->str:
        """web_api"""
        return "web_api"

    # pylint: disable=too-many-arguments
    def expect_content(self, url: str, content: str, method="GET",
                       headers: StrDict = None,
                       data: UrlData = None,
                       params: UrlParam = None,
                       timeout: int = None)->float:
        """ Compares text from the request with content variable.
        :return: value between 0 and 1. 1 means that contents are the same.
        """
        req = self.send(url, method, headers, data, params, timeout)
        seq = difflib.SequenceMatcher(None, content, req.text)
        return seq.ratio()

    # pylint: disable=too-many-arguments

    def expect_page(self, url: str, expected_url: str, method="GET",
                    headers: StrDict = None,
                    data: UrlData = None,
                    params: UrlParam = None,
                    timeout: int = None)->bool:
        """ check if url is the one expected and there is no error status code
        """
        expected_url = web.create_url(url, scheme=self._base_scheme,
                                      netloc=self._base_netloc).geturl()
        req = self.send(url, method, headers, data, params, timeout)
        return (req.url == expected_url) and (req.status_code < 400)

    # pylint: disable=too-many-arguments
    def expect_json(self, url: str, content: Union[str, StrDict], method="GET",
                    headers: StrDict = None,
                    data: UrlData = None,
                    params: UrlParam = None,
                    timeout: int = None)->bool:
        """ Compares json from the request with content variable.
        :return: True if same content. false otherwise
        """
        if isinstance(content, str):
            content = json.loads(content)
        req = self.send(url, method, headers, data, params, timeout)
        return content == req.json()

    # pylint: disable=too-many-arguments
    def expect_status(self, url, status, method="GET",
                      headers: StrDict = None,
                      data: UrlData = None,
                      params: UrlParam = None,
                      timeout: int = None)->bool:
        """ Sends request and returns boolean according to the expected http
        status code
        :return: True if same http status, False otherwise
        """
        timeout = timeout or self.timeout
        req = self.send(url, method, headers, data, params, timeout)
        return req.status_code == status
