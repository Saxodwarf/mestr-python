"""Some generic steps that can be used with several Managers."""
import logging
import time
from behave.runner import Context
from behave.model import Status
from behave import *
from mestr.utils.exceptions import MestrTimeout


@When("I set '{manager}' to '{status}' state")
@When("I set '{manager}' '{status}'")
@Given("'{manager}' has to be in '{status}' state")
@Given("'{manager}' has to be '{status}'")
def step_impl(context: Context, status: str, manager: str):
    """ Test the state of the manager. If the state is not the expected one,
    set it.

    :param manager: manager to check/set the status
    :param status: status to check/set

    """
    if not context.managers[manager].is_state(status):
        context.managers[manager].set_state(status)


@Given("'{manager}' is '{status}'")
@Then("'{manager}' should be in '{status}' state")
@Then("'{manager}' should be '{status}'")
def step_impl(context: Context, status: str, manager: str):
    """Given('I am "{status}" from "{manager}"')
    Checks if a Manager is in a particular status.

    :param manager: manager name
    :param status: status to test
    """
    logging.info("STATUS : manager : " + manager + " status : " + status)
    assert context.managers[manager].is_state(status),  \
        "{} has not currently the status {}".format(manager, status)


@Then("I wait '{delay:g}' seconds")
@When("I wait '{delay:g}' seconds")
def step_impl(context: Context, delay: float):
    """Sleeps for a while

    :param delay: time in seconds to wait
    """
    logging.info("Waiting {} seconds".format(delay))
    time.sleep(delay)


@When("'{manager}' sends '{data}'")
def step_impl(context: Context, data: str, manager: str):
    """Send data through a manager.
    Can be used with any Manager providing the "send" method.

    :param data: data to send
    :param manager: manager name
    """
    logging.info("SEND : manager : " + manager + " data : " + data)
    context.managers[manager].send(data)


@When("'{manager}' receives '{data}'")
@When("'{manager}' receives '{data}' in the '{timeout:d}' following seconds")
@Then("'{manager}' should receive '{data}'")
@Then("'{manager}' should receive '{data}' in the '{timeout:d}' following seconds")
def step_impl(context: Context, data: str, manager: str, timeout: int = 120):
    """Expect some data from a manager.
    Can be used with any Manager providing the "expect" method

    :param data: data expected
    :param manager: manager name
    :param timeout: timeout to expect data
    """
    try:
        context.managers[manager].expect(data, timeout=timeout)
    except MestrTimeout as time_excpt:
        msg = "Timeout excedeed ({0})".format(timeout)
        assert False, msg


@Given("'{manager}' has to be working")
def step_impl(context: Context, manager: str):
    """ This step is working according to integrity tests it will check
    if the manager is working according to integrity test result.
    Considere manager as working if the status is not failed.
    (should only be used for integrity tests)

    :param manager: Manager that should be working
    """
    msg = "manager {} has status failed".format(manager)
    assert context.integrity[manager] != Status.failed, msg
