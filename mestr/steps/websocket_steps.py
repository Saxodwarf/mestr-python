import logging
import time
from behave import *
from behave.runner import Context


@When("I 'connect' to '{manager}' using '{api}' manager")
def step_impl(context: Context, manager: str, api: str):
    """When('I "{action}" to/from "{manager}" using "{api}" manager')
       method to connect to websocket manager using api authentication cookies

    :param manager: websocket manager name
    :param api: web_api manager name
    """
    logging.info("CONNECTION manager : " + manager)
    cookies = context.managers[api].session.cookies
    context.managers[manager].login(cookies=cookies)
