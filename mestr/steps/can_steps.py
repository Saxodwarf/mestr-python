"""Some CAN specific  steps"""
import time
from behave import *
from behave.runner import Context
from pexpect import TIMEOUT

from mestr.utils.exceptions import MestrTimeout


@When("I send the CAN packet '{packet}' from the DUT using '{manager}'")
def step_impl(context: Context, packet: str, manager: str):
    """Sends a packet *from* the can0 interface of the DUT, using
    the cansend utility. For the step to work, a connected shell manager
    (serial console or ssh for instance) must be available, and the can-utils
    package must be installed on the DUT.

    :param packet: A CAN packet given in a valid format for cansend (see the
        cansend's man page for more informations)
    :param manager: The connected shell manager (serial ou ssh for instance)
    """
    context.managers[manager].sendline("cansend can0 {0}".format(packet))
    context.managers[manager].prompt()


@Then("I should receive '{packet}' on '{name}'")
def step_impl(context: Context, packet: str, name: str):
    """Step that expect a can packet on a can interface.

    :param name: CAN manager name to expect the packet on.
    :param packet: A CAN packet to expect.

        It must be given either as the payload only, as
        an hexadecimal string of at most 16 characters, or
        with the form: 'ID#DATA', with ID and DATA hexadecimal
        strings.
    """
    split_pack = packet.split("#")
    can_id = None
    if len(split_pack) == 2:
        can_id = int(split_pack[0], 16)
        data = bytes.fromhex(split_pack[1])
    else:
        data = bytes.fromhex(packet)
    try:
        context.managers[name].expect(can_id=can_id,
                                      data=data)
    except MestrTimeout:
        assert False, "CAN packet not received on {0}".format(name)


@When("I listen for CAN packets on the DUT using '{manager}'")
def step_impl(context: Context, manager: str):
    """Begins to dump the data received on the CAN bus of the DUT,
    using candump and a connected shell (serial or ssh for instance).

    It requires the can-utils package to be installed on the DUT to work

    :param manager: Manager handling the connected shell.
    """
    context.managers[manager].sendline("candump can0")
    time.sleep(0.5)


@Then("I should receive '{string}' on the DUT using '{manager}'")
def step_impl(context, string, manager):
    """Expects a CAN packet received by the DUT.

    It assumes that the DUT is currently listening for CAN packets,
    and printing them on a shell. It requires a connected shell.

    :param string: Representation of the expected output from the
        candump tool.
    :param manager: Manager handling the connected shell (serial or
        ssh)
    """
    i = context.managers[manager].expect([string, TIMEOUT])
    context.managers[manager].send("\x03")
    context.managers[manager].prompt()
    assert i == 0, "CAN packet never received on the DUT."
