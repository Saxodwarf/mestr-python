from behave import *
from behave.runner import Context
from mestr.utils.exceptions import MestrTimeout


@Given("'{name}' is able to reach the web server")
@Then("'{name}' is able to reach the web server")
def step_impl(context: Context, name: str):
    """
    send a request to the **url** set on the webManager and expect a 200 status

    :param name: web_api manager name
    """
    try:
        assert context.managers[name].expect_status("", 200), \
            "return status not 200"
    except MestrTimeout:
        assert False, "Connection timed out"


@When("I 'connect' to '{name}' with '{username}' and '{password}' credentials")
def step_impl(context, name: str, username: str, password: str):
    """When("I 'connect' to '{name}' with '{username}' and '{password}'\
    credentials")
       method to connect to websocket or web_api manager using specific
       username and password

    :param name: websocket or web_api manager name
    :param username: username
    :param password: password
    """
    context.managers[name].auth.username = username
    context.managers[name].auth.password = password
    context.managers[name].login()


@Then("'{name}' should be able to visit '{url}'")
def step_impl(context: Context, url: str, name: str):
    """Then("'{name}' should be able to visit '{url}'")
    try to reach an url with web_api. Test the return  code and
    if the url is the same as the one requested

    :param name: web_api manager name
    :param url: expected url
    """
    msg = "wrong status or url"
    assert context.managers[name].expect_page(url, url), msg
