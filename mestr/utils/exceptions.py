"""Module to define some customs exceptions"""


class MestrTimeout(Exception):
    """Raised for timeouts when a Manager expects some data"""
