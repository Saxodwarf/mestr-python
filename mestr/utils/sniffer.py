""" Module to help sniffing dhcp packets and analyse them """
import os
import shutil
import random
from subprocess import run, TimeoutExpired, PIPE
from typing import Optional, List, Dict, Callable, Tuple, Union
import dpkt


def read_packets(pcap_file_path: str)-> List[Tuple[float, bytes]]:
    """Read network packets from a pcap file and returns a
    list of packets.

    :param pcap_file_path: Path to the pcap file to read.
    :return: A list of (timestamp, raw_packet) tuples.
    """
    pckts: List[Tuple[float, bytes]] = []
    if os.path.isfile(pcap_file_path):
        if os.stat(pcap_file_path).st_size > 0:
            with open(pcap_file_path, "rb") as pcap_file:
                reader = dpkt.pcap.Reader(pcap_file)
                pckts = reader.readpkts()
    return pckts


def new_name(old_name: str)->str:
    """Generate a filename based on a previous one, that doesn't exists
    at the moment of it's creation, and that has a 5-letter random
    string in it.

    If old_name does not exists already, it is simply returned as the new name.

    Example: ::

        # If a file named "toto.txt" does exists:
        n_name = new_name("toto.txt")
        print(n_name)
        # Prints something like:
        # toto_dmsjf.txt

        # If no file named "toto.txt" exists:
        n_name = new_name("toto.txt")
        print(n_name)
        # Prints this:
        # toto.txt

    :param old_name: base string for the new name.
    """
    new_path = old_name
    while os.path.exists(new_path):
        component = list(old_name.partition("."))
        rdm_str = "".join(
            ["_"]+[chr(random.randint(ord('a'), ord('z'))) for i in range(5)])
        component.insert(-2, rdm_str)
        new_path = "".join(component)
    return new_path


class BasicSniffer:
    """ Class to help sniffing network packets and analyse them"""

    def __init__(self, interface: str,
                 timeout: int = 20, log: Callable = None):
        """Class constructor

        :param interface: interface on which to listen
        :param timeout: time before the function returns (default None)
        :param log: callback to a logging function, that takes a string
            to log as parameter.
        """
        self._output_file: str = ""
        self.pckts: Union[List[dpkt.Packet], List[bytes]] = []
        self.interface = interface
        self.timeout = timeout
        self.manager_log = log

    def set_output_file(self, path_pcap: str)->None:
        """Sets the output file of the sniffer

        :param path_pcap: Path to the output file.
        """
        self._output_file = path_pcap

    def get_output_file(self)-> str:
        """Returns the output file of the sniffer"""
        return self._output_file

    def sniff(self,
              count=16,
              cap_filter: str = "",
              offline: Optional[str] = None)->List[dpkt.Packet]:
        """Sniffs network packets

        It sniffs packets from the network or from a pcap file, process them
        using the **analize** method and puts them in the variable **pckts**.

        :param offline: path to a pcap file to be read. If specified,
                        a pcap file is read rather than actual packets
                        from the network.
        :param cap_filter: capture filter for tcpdump (no capture filter
                           by default).
        :param count: number of packet to sniff before returning (default 16)
        :return: list of the captured packets.

        """
        capture_filter = cap_filter
        old_offline = ""
        output_file = self._output_file or "captured_packets.pcap"

        cmd = ["tcpdump"]
        if offline:
            if output_file == offline:
                # tcpdump can't read from and write to the same file.
                # So we copy the source file to a temporary name,
                # with a random string in it.
                old_offline = offline
                offline = new_name(offline)
                shutil.copy2(old_offline, offline)
            cmd.extend(["-r", offline])
        else:
            cmd.extend(["-i", self.interface])
        cmd.extend(["-U", "-w", output_file, "-c", str(count), capture_filter])

        try:
            self.log("cmd = "+" ".join(cmd))
            comp_proc = run(cmd, timeout=self.timeout,
                            stdout=PIPE, stderr=PIPE)
            self.log("cmd stdout: " + comp_proc.stdout.decode("utf-8"))
            self.log("cmd stderr: " + comp_proc.stderr.decode("utf-8"))
        except TimeoutExpired as excpt:
            self.log("cmd stdout: " + excpt.stdout.decode("utf-8"))
            self.log("cmd stderr: " + excpt.stderr.decode("utf-8"))
        finally:
            if old_offline and offline and os.path.isfile(offline):
                # If old_offline is not an empty string, it
                # means that the current offline variable
                # contains the path of the temp file that has
                # to be removed.
                os.remove(offline)
        raw_pckts = read_packets(output_file)
        self.pckts = self.analize(raw_pckts)

        return self.pckts

    def analize(self, pckts: List[Tuple[float, bytes]]
                )-> Union[List[dpkt.Packet], List[bytes]]:
        """Can be overriden to give analizis capabilities
        to a subclass (parsing the packets, sorting them ...)

        This default method only logs how many packets were captured.

        :param pckts: List of tuple of timestamp and raw packets (bytestrings)
            to analize
        :return: List of the processed packets, to be stored in self.pckts
        """
        self.log("Got {0} packets".format(len(pckts)))
        return [pckt[1] for pckt in pckts]

    def log(self, data: str)->None:
        """Add the name of the class to the data to log, and write it
        to the logfile of the manager using a callback function.
        If no callback function is given, prints the information.

        :param data: Information to log.
        """
        message = type(self).__name__+": "+data
        if self.manager_log:
            self.manager_log(message+"\n")
        else:
            print(message)


class DhcpSniffer(BasicSniffer):
    """Class to help sniffing dhcp packets and analyse them

    To help manipulating the captured dhcp packets, they are grouped by XID
    in the **self.grouped_packets** dictionnary.
    Each entry in this dictionnary is called a transaction in the following
    documentation.

    The captured packets are also available in the **self.pckts** list.
    """

    def __init__(self, interface: str,
                 timeout: int = 20, log: Callable = None):

        super().__init__(interface=interface, timeout=timeout, log=log)
        self.grouped_packets: Dict[int, List[dpkt.dhcp.DHCP]] = {}
        self.xid_list: List[int] = []
        self.pckts: List[dpkt.dhcp.DHCP] = []

    def sniff(self,
              count=16,
              cap_filter="",
              offline: Optional[str] = None)->List[dpkt.dhcp.DHCP]:
        """Sniffs DHCP packets

        It is a wrapper around the BasicSniffer sniff() method, that
        forces a "udp port 67 or port 68" capture filter. It should not be
        changed, since the DHCP protocol communicates only on those
        ports, as written in RFC 2131. However, you can add additonnal filters
        using the cap_filter parameter. Please note that if you catch a packet
        which is not a dhcp packet, the analize method is **very** likely
        to fail.


        It sniffs packets from the network or from a pcap file, and puts them
        in the  variable **pckts**.

        :param offline: path to a pcap file to be read. If specified,
                        a pcap file is read rather than actual packets
                        from the network.
        :param count: number of packet to sniff before returning (default 16)
        :param cap_filter: additionnal capture filter for tcpdump to add to the
                           "udp port 67 or port 68" default.

        :return: list of the captured packets.

        """
        capture_filter = "udp port 67 or port 68 "+cap_filter
        return super().sniff(count=count,
                             offline=offline,
                             cap_filter=capture_filter)

    def analize(self, pckts: List[Tuple[float, bytes]])-> List[dpkt.dhcp.DHCP]:
        """Parses the raw packet as DHCP packets.

        It groups the packets in the **grouped_packets** dictionnary,
        according to their XID.

        :param pckts: List of tuples containing the timestamps and
                      raw network packets to process

        :return: List of the obtained packets.
        """
        dhcp_pckts = []
        self.xid_list = []
        self.grouped_packets = {}
        for packet in pckts:
            ethernet = dpkt.ethernet.Ethernet(packet[1])
            # ethernet.data.data.data : Ethernet->IP->UDP->Bootp/DHCP
            dhcp_pckts.append(dpkt.dhcp.DHCP(ethernet.data.data.data))
        self.group_by_xid(dhcp_pckts)
        self.log("Got {0} packets, and {1} unique XIDs"
                 .format(len(dhcp_pckts), len(self.xid_list)))
        return dhcp_pckts

    def group_by_xid(self, pckts: Optional[List[dpkt.dhcp.DHCP]] = None
                     )->List[int]:
        """ Groups the packets by XID

        Groups DHCP packets in a dictionnary of packet lists,
        using the packets XID as the key.

        It updates self.xid_list and self.grouped_packets.

        The packets can be given as parameter. If no list is passed,
        then the packets from self.pckts are used.

        :param pckts: An optionnal list of dhcp packets.
        :returns: a list of the found XIDs (or None if no packets in pckts)
        """
        list_pckts = pckts if pckts is not None else self.pckts
        xid_list = []
        if list_pckts:
            self.grouped_packets = {}
            for pkt in list_pckts:
                xid = pkt.xid
                if xid in self.grouped_packets:
                    self.grouped_packets[xid].append(pkt)
                else:
                    self.grouped_packets[xid] = [pkt]
                    xid_list.append(xid)
            self.xid_list = xid_list
        return xid_list

    def get_xid_list(self)->List[int]:
        """ Returns the list of XIDs """
        return self.xid_list

    def get_dhcp_transac(self, xid: int)->List[dpkt.dhcp.DHCP]:
        """ Returns the transaction of XID 'xid'.

        If no such transaction exists, returns an empty list.

        :param xid: XID of the transaction to return.
        """
        if not self.grouped_packets:
            self.group_by_xid()
        if xid in self.grouped_packets:
            return self.grouped_packets[xid]
        return []

    def _get_dhcp_option_dict(self, pckt: dpkt.dhcp.DHCP)->Dict[int, bytes]:
        """Returns the dhcp options of a packet, in a dictionnary

        This output dictionnary uses the options number as keys, and the
        corresponding values as values.

        :param pckt: packet to get the options from.
        :returns: a dictionnary containing the dhcp options of the packet.

        """
        option_list: List[Tuple[int, bytes]] = []
        if self.pckts:
            i: Tuple[int, bytes]
            for i in pckt.opts:
                if isinstance(i, tuple):
                    option_list.append(i)
        return dict(option_list)

    def get_dhcp_msg_type(self, xid: int, pckt_nm: int)-> int:
        """Returns the dhcp message type of a packet.

        The packet is selected using its xid, and its place in the transaction
        (starting at 0). the messages should sorted by order of capture, but
        it is not guaranteed.

        :param xid: xid of the transaction containing the desired packet.
        :param pckt_nm: place of the packet in the transaction.
        :return: the dhcp message type of the packet.

        """
        dhcp_options: Dict[int, bytes] = {}
        conv = self.get_dhcp_transac(xid)
        if len(conv) > pckt_nm >= 0:
            dhcp_options = self._get_dhcp_option_dict(conv[pckt_nm])
        if 53 in dhcp_options:
            return int(dhcp_options[53].hex(), 16)
        msg = "The selected packet does not contain a DHCP_MSG_TYPE option."
        raise ValueError(msg)

    def show_grouped_by_xid(self, log: bool = False)->None:
        """Displays the captured packets content, grouped by XID.

        It simply prints the groups one by one to the standard output, or logs
        them using the **self.log** callback method, if it is defined and the
        **log** parameter is True.

        :param log: If true, will try to use the **self.log** method (if it is
            defined) to print the output to a log file.
        """
        if not self.grouped_packets:
            self.group_by_xid()
        if log:
            for xid in self.grouped_packets:
                self.log("XID : " + str(xid))
                for pkt in self.grouped_packets[xid]:
                    self.log(repr(pkt))
        else:
            for xid in self.grouped_packets:
                print("XID : ", xid, "\n")
                for pkt in self.grouped_packets[xid]:
                    print(repr(pkt))
