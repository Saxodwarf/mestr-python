"""create aliases for common type in our project"""
from typing import Union, AnyStr, Pattern, List, Dict

Expect = Union[AnyStr, Pattern, List[Union[AnyStr, Pattern]]]
StrDict = Dict[str, str]
UrlParam = Dict[str, Union[str, List[str]]]
UrlData = Union[tuple, Dict[str, Union[str, List[str]]]]
