"""Decorators function to add mestr needed behavior to behave"""
import datetime
import mimetypes
import os
from enum import Enum
from typing import Callable, List
import allure
from behave import fixture, use_fixture
from behave.runner import Context
from behave.model import Feature, Scenario, Status


_MANAGER_PREFIX = "manager."
_INTEGRITY_PREFIX = "fixture.integrity"
_INTEGRITY_TAG = "integrity"


# pylint: disable=protected-access
def mestr_before_all(func: Callable)-> Callable:
    """Decorator function
    set managers object and tag list to context.

    Check the existensy of integrity tests.
    """
    def wrapper(context: Context) -> Callable:
        # move managers object from config to context
        context.managers = context.config.managers
        del context.config.managers
        # move logfile path from config to context
        if hasattr(context.config, "log_files_path"):
            context.log_files_path = context.config.log_files_path
            del context.config.log_files_path
        # create a list of available managers
        context.man_tags = list(_MANAGER_PREFIX + x
                                for x in context.managers.keys())
        # create a list for integrity tests
        context.integrity = dict.fromkeys(
            context.managers.keys(), Status.untested)
        # check if there is integrity feature and put it at the beginning of
        # the list
        integrity_features: List[Feature] = list()
        for feature in context._runner.features:
            if _INTEGRITY_TAG in feature.tags:
                # feature uses name as comparator so don't give the same name
                # for two features
                integrity_features.append(feature)
                context._runner.features.remove(feature)
        context._runner.features[0:0] = integrity_features
        return func(context)
    return wrapper


def manager_tags(tag_list: List[str])->List[str]:
    """Returns a list contaning all the "manager.<name>" tags contained in
    the input list
    """
    ret_lst = list()
    for tag in tag_list:
        if tag.startswith(_MANAGER_PREFIX):
            ret_lst.append(tag)
    return ret_lst


def mestr_before_feature(func: Callable) -> Callable:
    """Decorator function. Action executed before each feature
    assure that required material is present.
    """
    def wrapper(context: Context, feature: Feature)-> Callable:
        if not is_superlist(context.man_tags, manager_tags(feature.tags)):
            feature.skip("some tag missing")
        return func(context, feature)
    return wrapper


def mestr_before_scenario(func: Callable) -> Callable:
    """Decorator function. Action executed before each scenario
    assure that required material is present
    """
    def wrapper(context: Context, scenario: Scenario)-> Callable:
        if not is_superlist(context.man_tags, manager_tags(scenario.tags)):
            scenario.skip("some tag missing")
        else:
            use_fixture(log_managers, context, scenario.name)
        return func(context, scenario)
    return wrapper


def mestr_after_scenario(func: Callable)->Callable:
    """Decorator function.
    Empty buffer of all managers between scenarios and store the result of the
    test if it's an integrity one
    """
    def wrapper(context: Context, scenario: Scenario)-> Callable:
        if "integrity" in context.tags:
            # get only the first occurence of manager name
            try:
                man_tag = [e for e in scenario.tags if _MANAGER_PREFIX in e][0]
                context.integrity[man_tag.split(".")[-1]] = scenario.status
            except IndexError as err:
                err.args = ("no manager found",)
                raise
        for man_name, man in context.managers.items():
            if _MANAGER_PREFIX + man_name in context.tags:
                man.clear_input()
        return func(context, scenario)
    return wrapper


def mestr_after_feature(func: Callable)->Callable:
    """Decorator function.
    test if it's an integrity feature, remove manager tag where test fails
    """
    def wrapper(context: Context, feature: Feature)-> Callable:
        if "integrity" in feature.tags:
            for man_name, status in context.integrity.items():
                if status == Status.failed:
                    context.man_tags.remove(_MANAGER_PREFIX + man_name)
        return func(context, feature)
    return wrapper


def is_superlist(superlist: list, sublist: list) -> bool:
    """  check if one list is a subset of the other
    """
    return all(x in superlist for x in sublist)


@fixture
def log_managers(context: Context, scenario_name: str)->None:
    """ fixture to manage logging in behave.

    create a file to store logs and manage it according to the formatter
    :param context: behave context
    :param scenario: current scenario
    """
    path = ""
    if hasattr(context, "log_files_path"):
        path = context.log_files_path
    date = datetime.datetime.now().replace(microsecond=0).isoformat()
    name = scenario_name.title().replace(" ", "").replace(".", "-")
    managers_used = list()
    for man_name, man in context.managers.items():
        if _MANAGER_PREFIX + man_name in context.tags:
            managers_used.append(man_name)
            man.start_logging(path + "_".join([date, man_name, name]))
    yield
    for man_name in managers_used:
        file_names = context.managers[man_name].stop_logging()
        if ("allure_behave.formatter:AllureFormatter"
                in context.config.format):
            if isinstance(file_names, str):
                file_names = [file_names]
            for file_name in file_names:
                allure_type = get_allure_type(file_name)
                if os.path.isfile(file_name):
                    if os.stat(file_name).st_size > 0:
                        allure.attach.file(file_name, man_name+"_output",
                                           allure_type)
                    os.remove(file_name)


def get_allure_type(file_loc: str)->Enum:
    """Return the allure type that have to be sepcified for attachement
    accoding to the extension
    """
    file_name = file_loc.rsplit("/", 1)[-1]
    mime_type = mimetypes.guess_type(file_name)[0] or 'text/plain'
    split_name = file_name.rsplit(".", 1)
    if len(split_name) >= 2:
        suffix = split_name[-1]
    else:
        suffix = "txt"
    # pylint: disable=no-value-for-parameter
    return allure.attachment_type((mime_type, suffix))
