"""Helpers for authentication"""
import os
from typing import Tuple, Optional


def get_creds_from_env(auth_creds: str)->Tuple[Optional[str], Optional[str]]:
    """Get the credentials from the environment.

    The variables must have the format <auth_creds>_username and
    <auth_creds>_password

    :param auth_creds: Prefix for the credential name.
    :return: Tuple in the form :  (username, password)
    """
    username = os.getenv(auth_creds + "_username")
    password = os.getenv(auth_creds + "_password")
    return (username, password)
