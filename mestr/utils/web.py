""" Module with some utils functions"""
import importlib
from typing import Optional, Callable
from urllib.parse import urlsplit, SplitResult


def create_url(url: str, **kwargs: Optional[str])->SplitResult:
    """
    Gets url like ws://example.com/foo/bar  localhost/foo/bar or /foo/bar
    and returns a SplitResult object
    :param url: base url to prepare
    :param kwargs: default part url if value not found by parsing url
    :return: Split result from url
    """
    url_dict = urlsplit(url)._asdict()
    if not url_dict["netloc"]:
        url_dict = urlsplit("//" + url)._asdict()
    for arg in kwargs.items():
        if not url_dict[arg[0]]:
            url_dict[arg[0]] = arg[1]
    return SplitResult(**url_dict)


def update_url(url: SplitResult, **kwargs: str)->SplitResult:
    """
    Get url as splitResult object and update it with kwargs parameters
    :param url: base url to prepare
    :param kwargs: url parameters to override
    :return:  updated result from url
    """
    url_dict = url._asdict()
    for arg in kwargs.items():
        url_dict[arg[0]] = arg[1]
    return SplitResult(**url_dict)


def parse_class(auth_str: str)->Callable:
    """   parse str class and return class object implementation.
    this function take a string like foo.bar(arg1=val1, arg2=val2...)
    and create an instance of the class bar with given arguments

    :param auth_str: string with class informations. arguments will be
        considered as string
    :return: class instance created form string

    """
    lst = auth_str.split('.')
    # get class name and arguments
    cls_value = lst.pop()
    # import class
    mod = importlib.import_module(".".join(lst))
    # split class name and each argument
    cls_list = cls_value.replace("(", ",").replace(")", "").split(",")
    # get class name
    class_ = getattr(mod, cls_list.pop(0))
    kwargs_dict = dict()
    kwargs_lst = list()
    for arg in cls_list:
        arg = arg.replace("\'", "").replace("\"", "")
        split = arg.split("=")
        if len(split) == 2:
            kwargs_dict[split[0]] = split[1]
        elif split[0]:
            kwargs_lst.append(split[0])
    return class_(*kwargs_lst, **kwargs_dict)
