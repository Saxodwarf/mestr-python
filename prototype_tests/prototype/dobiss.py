from os import getenv
import re
import time
from typing import Dict, Any
from requests.auth import AuthBase
from requests import Session, Request, models
from mestr.managers.serial_man import SerialManager


class DobissAuth(AuthBase):
    """manage dobiss authentification with csrf field"""

    regex = re.compile('.*value="(\\w*)".*')

    def __init__(self, username: str = None, password: str = None, env_auth: str = None):
        env_auth = str(env_auth)
        self.username = getenv(env_auth + "_username", username)
        self.password = getenv(env_auth + "_password", password)
        self.authentication: Dict[str, Any] = dict()

    def __eq__(self, other: object):
        return all([
            self.username == getattr(other, 'username', None),
            self.password == getattr(other, 'password', None),
        ])

    def __ne__(self, other: object):
        return not self == other

    def __call__(self, r: models.PreparedRequest):
        body = dict()
        s = Session()
        login_page = s.get(r.url)
        for line in login_page.iter_lines():
            line_str = str(line)
            if "csrf"in line_str:
                csrf_value = re.match(self.regex, line_str)
                if csrf_value:
                    body["csrf"] = csrf_value.group(1)
                    body["password"] = self.password
                    body["user"] = self.username
                else:
                    raise ValueError("csrf didn't match")
                req = Request(r.method, r.url, data=body)
                req.cookies = login_page.cookies
                self.authentication["cookies"] = login_page.cookies
                return s.prepare_request(req)

        raise ValueError("csrf field not found")


def set_stable_state(shell: SerialManager) -> None:
    """ Resets the DUT database.

    Stops the mysql deamon, then deletes the /data/mysql directory.

    :param shell: a logged in shell to the DUT.
    """
    print("reset mySQL")
    shell.sendline("start-stop-daemon -K -n mysqld")
    time.sleep(15)
    shell.prompt()
    shell.sendline("rm -rf /data/mysql/")
    shell.prompt()
    shell.sendline("reboot")


def clean_web_log(req: Request)-> Request:
    """ remove informations about login and password used to connect to dobiss

    :param req: the Request to clean_web_log
    :return : request without sensitive or useless data
    """
    if hasattr(req, "body") and req.body:
        body = req.body.split("&")
        out = list()
        for el in body:
            if "password" in el:
                out.append("password=******")
            elif "user" in el:
                out.append("user=******")
            else:
                out.append(el)
        req.body = "&".join(out)
    return req
