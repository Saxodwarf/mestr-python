""" file defining action around behave event"""
import time
import os
import re
import allure
from pexpect import TIMEOUT
from behave import fixture, use_fixture
from behave.runner import Context
from behave.model import Feature, Scenario, Tag, Status
from prototype_tests.prototype.dobiss import set_stable_state
from mestr.utils.base_env import (mestr_before_all, mestr_after_scenario,
                                  mestr_before_feature, mestr_before_scenario, mestr_after_feature)


@mestr_before_all
def before_all(context: Context)->None:
    """function executed before all tests are run"""
    pass


@mestr_before_feature
def before_feature(context: Context, feature: Feature) -> None:
    """function executed before all feature"""
    pass


@mestr_before_scenario
def before_scenario(context: Context, scenario: Scenario) -> None:
    """function executed before each scenario"""
    pass


@mestr_after_scenario
def after_scenario(context, scenario):
    """function executed after each scenario"""
    pass


@mestr_after_feature
def after_feature(context, feature):
    """function executed after each feature"""
    pass


def before_tag(context: Context, tag: Tag) -> None:
    """action executed for each tag
    calls the needed fixtures
    """
    if tag == "fixture.stable_state":
        use_fixture(stable_state, context)
    elif tag == "fixture.must_boot":
        use_fixture(must_boot, context)


@fixture
def must_boot(context: Context) -> None:
    """finish the boot process even if the test fails. Device depedant"""
    yield
    boot_string = "Dobiss system"
    if context.scenario.status == Status.failed:
        context.managers["debug"].expect(boot_string, timeout=75)


@fixture
def stable_state(context: Context) -> None:
    """Fixture to put the manager under test in a known
    state before running the test.
    It takes a long time to execute, as the DUT has to
    boot several time...
    This fixture is device-specific : the boot_string, error_re,
    err_tolerance ... must be adapted to the device to be tested.
    """
    yield
    if context.scenario.status != Status.skipped:
        with open("stable_state.log", "w") as logfile:
            # Begin : variable setup
            boot_string = "Dobiss system"
            error_string = re.compile("^.*[eE]rror.*$", re.MULTILINE)
            # HACK : there may be some errors unrelated to what we want to
            # do, so we are a bit tolerant.
            err_tolerance = 2
            username = os.getenv("shell_login", None)
            password = os.getenv("shell_password", None)
            ser = context.managers["debug"]
            power_supp = context.managers["power"]
            ser.logfile_read = logfile
            # End : variable setup

            power_supp.set_value(power_supp.LOW)
            time.sleep(2)

            # Begin : normal boot
            power_supp.set_value(power_supp.HIGH)
            i = ser.expect([TIMEOUT, boot_string], timeout=120)
            if i == 0:
                assert False, "Timeout excedeed ({})".format(120)
            # End : normal boot
            time.sleep(2)
            # Begin : login
            ser.login(username=username, password=password)
            # End : login

            # Begin : resetting the DUT
            set_stable_state(ser)
            # End : resetting the DUT

            # Begin : wait for DUT to reboot, and warn for errors
            i = -1
            err_cnt = 0
            while i != 1:
                i = ser.expect([TIMEOUT, boot_string, error_string],
                               timeout=120)
                if i == 0:
                    assert False, "Timeout excedeed ({})".format(120)
                if i == 2:
                    err_cnt += 1

            if err_cnt > 0:
                print("WARNING : got error(s) during reboot")
            error_msg = "Got {0} errors during reboot".format(err_cnt)
            assert err_cnt <= err_tolerance, error_msg
            # End : wait for DUT to reboot, and warn for errors

            # Begin : login
            ser.login(username=username, password=password)
            # End : login
            # Begin : shutdown the system cleanly
            ser.sendline("halt")
            i = ser.expect([TIMEOUT, "System halted"], timeout=60)
            if i == 0:
                error_msg = "System did not shut down in time ({0})".format(60)
                assert False, error_msg
            # End : shutdown the system cleanly

            time.sleep(2)
            power_supp.set_value(power_supp.LOW)
            time.sleep(2)
            ser.logfile_read = None
        if ("allure_behave.formatter:AllureFormatter"
                in context.config.format):
            allure.attach.file("stable_state.log",
                               "stable_state output",
                               allure.attachment_type.TEXT)
