@manager.network @manager.can @manager.ws @manager.power_relay @manager.pairing_button @manager.api
Feature: testing the relay

Background: connecting to the DUT
   Given 'api' has to be 'logged in'
     And 'power_relay' has to be 'on'
     And 'ws' has to be 'opened'
     And I address a module using websocket 'ws' and 'pairing_button'


@normal @manager.external_gpio
Scenario: closing the GPIO-controlled relay
   Given 'external_gpio' is 'low'
    When 'ws' sends '[2, "0.zhmfxx48ebc", "com.dobiss.commands/action", 1, 11, 1, 100, null]'
    Then I should receive '05FC0102#010B01FFFF64FFFF' on 'can'
     And 'external_gpio' should be 'high'

@manager.relay_gpio
Scenario: closing a relay
   Given 'relay_gpio' is 'low'
    When 'ws' sends '[2, "0.zhmfxx48ebc", "com.dobiss.commands/action", 1, 7, 1, 100, null]'
     And I wait '0.3' seconds
    Then I should receive '05FC0102#010701FFFF64FFFF' on 'can'
     And I wait '0.2' seconds
     And 'relay_gpio' should be 'high'
