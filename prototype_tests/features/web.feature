@manager.api
Feature: testing web connection

Scenario: connection
   Given 'api' is able to reach the web server
   Given 'api' has to be 'logged out'
   When I set 'api' to 'logged in' state
   Then 'api' should be able to visit '/config'
   
@manager.ws
Scenario: test web socket
    Given 'api' has to be 'logged in'
    When I 'connect' to 'ws' using 'api' manager
    Then 'ws' should receive 'Connection accepted: session data found in cache'
    When 'ws' sends '[2,"0.tx95wcpo6pf", "com.dobiss.config/settings/users/save", { "name": "foo", "email": "test@test.de", "password": "sdfghbvc", "language": "en_UK", "profilesId": "1" } ]'
   Then 'ws' should receive '[3,"0.tx95wcpo6pf","User updated"]'

Scenario: use new user
    Given 'api' has to be 'logged in'
    When I set 'api' to 'logged out' state 
    And I 'connect' to 'api' with 'foo' and 'sdfghbvc' credentials
    Then 'api' should be able to visit '/config'
