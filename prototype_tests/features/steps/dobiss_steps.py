import time
from pexpect import TIMEOUT
from behave import *
from mestr.steps import *
from behave.runner import Context
from behave.model import Status


@Given("Dobiss has to be booted")
def step_impl(context: Context):
    """ totally dobiss dependant step. It assume that there is a serial manager
    called debug. If we can read the prompt or the login, we assume the device is booted.
    """
    if context.managers["debug"].is_state("logged out"):
        context.managers["debug"].sendline("")
        if context.managers["debug"].expect(["login: ", TIMEOUT], 1):
            context.execute_steps("Given 'power' has to be 'off'")
            context.execute_steps("When I set 'power' 'on'")
            context.execute_steps("Then 'debug' should receive 'Dobiss system' in the '120' following seconds")


@given("I address a module using websocket '{websocket}' and '{button}'")
def step_impl(context, websocket, button):
    """Step that uses the websocket interface to associate a module to the server.

    :param websocket: The name of the websocket manager.
    :param button: The name of the relay manager simulating the button.
    """
    relay_but = context.managers[button]
    ws = context.managers[websocket]
    time.sleep(1)
    ws.send('[2,"0.bskntmczf3n", "com.dobiss.config/modules/address/start"]')
    time.sleep(3)
    relay_but.set_value(relay_but.HIGH)
    time.sleep(1)
    relay_but.set_value(relay_but.LOW)
    time.sleep(3)
    ws.send('[2,"0.bskntmczf3n", "com.dobiss.config/modules/address/stop"]')
