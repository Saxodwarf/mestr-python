    # Tag list to run only test with available module
    @manager.power @manager.debug
    Feature: testing boot process
        this feature test the boot process. It first try a boot scenario without 
        trouble and secondly a boot scenario with a power cut during boot
        process
    
    @reset
    @fixture.stable_state
    Scenario Outline: boot with trouble
        Given 'power' has to be 'off'
        When I set 'power' 'on'
        And I wait '<wait>' seconds
        And I set 'power' 'off'
        And I wait '1' seconds
        And I set 'power' 'on'
        Then 'debug' should receive 'Dobiss system' in the '120' following seconds
    
    Examples: times
        | wait |
        | 5    |
        | 35   |
