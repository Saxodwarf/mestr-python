@manager.network @manager.debug @manager.power
Feature: testing the dhcp client


@normal @broken
Scenario: ethernet cable disconnection
   Given 'network' has to be 'up'
    When the User disconnects and reconnects the cable from 'network'
    Then a DHCP operation should happen on 'network' within '15' seconds
