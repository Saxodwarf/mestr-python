"""Module containing the main function for Mestr."""
import sys
import argparse
import os
import tempfile
import stat
import fcntl
from typing import List, Optional
from behave.__main__ import run_behave
from behave.configuration import Configuration
from mestr import config_parser


def check_other_instance():
    """This function uses a locked file to check if an other instance of MESTR
    is already running, and exit if there is one.
    """
    app_name = 'mestr'

    # Lock file settings
    lf_name = '{}.lock'.format(app_name)
    lf_path = os.path.join(tempfile.gettempdir(), lf_name)
    lf_flags = os.O_WRONLY | os.O_CREAT
    lf_mode = stat.S_IWUSR | stat.S_IWGRP | stat.S_IWOTH  # This is 0o222

    # creating lock file
    umask_original = os.umask(0)
    try:
        lf_fd = os.open(lf_path, lf_flags, lf_mode)
    finally:
        os.umask(umask_original)

    # Try locking the file
    try:
        fcntl.lockf(lf_fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
    except OSError as excpt:
        if excpt.strerror == "Resource temporarily unavailable":
                sys.exit("An instance of MESTR is probably already running.")
        sys.exit("Error while locking...")


def main(console_args: List[str], config_file: Optional[str],
         man_path: Optional[str]):
    """Main function:

    Parses the config file, adds some arguments to behave's configuration,
    and calls Behave with the command line arguments.

    :param console_args: Command line argument list, can be empty.
    :param config_file: path to a config file. Must be an existing yaml
        file.
    """
    check_other_instance()
    data_parsed = config_parser.yaml_config(config_file, man_path)
    stage = str()
    feat_filter = str()
    tags: List[str] = list()

    # default feature directory : tests/features
    features = data_parsed.get("Features", "tests/features")

    if "Stage" in data_parsed:
        stage = " --stage {} ".format(data_parsed["Stage"])

    if "Formatter" in data_parsed and data_parsed["Formatter"]:
        formatter = " -f {} ".format(data_parsed["Formatter"])

    if "Output" in data_parsed and data_parsed["Output"]:
        output = " -o {} ".format(data_parsed["Output"])

    if "Filter" in data_parsed:
        if "include" in data_parsed["Filter"]:
            feat_filter += " -i {} ".format(data_parsed["Filter"]["include"])
        if "exclude" in data_parsed["Filter"]:
            feat_filter += " -e {} ".format(data_parsed["Filter"]["exclude"])

    for tag_line in data_parsed["Tags"]:
        tags.append(" --tags {} ".format(",".join(tag_line)))
    config = Configuration(stage +
                           " ".join(tags) +
                           formatter +
                           output +
                           feat_filter +
                           " ".join(console_args) +
                           " " + features)
    # store managers object
    config.managers = data_parsed["managers"]

    # Add log files path :
    if "Log_files" in data_parsed:
        config.log_files_path = data_parsed["Log_files"]
    return run_behave(config)


if __name__ == "__main__":
    # To use a custom config file, use "--config path/to/file.yml"
    PARSER = argparse.ArgumentParser(description="Mestr test platform")
    PARSER.add_argument("--man_path",
                        help="Path to managers configuration file")
    PARSER.add_argument("--config",
                        dest="config_file", help="Path to a config file")
    ARGS = PARSER.parse_known_args()

    sys.exit(main(ARGS[1], config_file=ARGS[0].config_file,
                  man_path=ARGS[0].man_path))
