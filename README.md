mestr-python (Mind's Embedded Software Test Rig)
================================================

![image](https://essensium-mind.gitlab.io/mestr/pylint.svg)
![image](https://essensium-mind.gitlab.io/mestr/mypy.svg)

This project is developed by the company [Mind](https://www.mind.be/), in order to propose a partial solution for generic embedded prototype testing.


The detailed documentation can be found [here](https://essensium-mind.gitlab.io/mestr).


Concept
-------

**Mestr** is a test platform that tries to be as generic as possible, in order
to be able to test software running on different devices with almost the same
setup and same panel of tests.

To do so, **Mestr** is based on a Beaglebone Black for the hardware part
and uses [Behave](https://behave.readthedocs.io/en/latest/) as a test
framework. It implements an abstraction layer to connect to the **DUT**'s
interfaces.

**Mestr** can then be connected to any continuous integration tool. During development,
we used gitlab-ci, because we are using gitlab to develop **Mestr**.

How does it work ?
-------------------

When a test job is launched by gitlab-ci, the gitlab-runner is triggered
in the beaglebone.

Then, **Mestr** reads a yaml file containing all the informations needed to instantiate
helper classes that manage connexions between the DUT and the platform. This yaml file
is stored on the test platform, and should not change a lot.

An other yaml file can be used to configure the test framework and select the
tests that will be run, but it is not mandatory. This configuration file can be
stored along the test cases.

After that, **Mestr** launches **behave** , and passes the helper classes as context
variable.

When **behave** stops, the reports are sent as artifacts to gitlab-ci.
These reports can then be processed by the Allure framework to create
a static web site with a nice representation of the reports.

### Architecture

![image](docs/imgs/mestr_organisation.png)

### Workflow

![image](docs/imgs/testExecution.png)

How to use MESTR ?
--------------------

> -   First, install all the requirements [on the
>     beaglebone](./docs/source/boneSetup.rst)
> -   Write a `managers.yml` file
> -   Write your
>     [features](https://behave.readthedocs.io/en/latest/tutorial.html)
>     files
> -   If needed implement your own
>     [steps](https://behave.readthedocs.io/en/latest/api.html#step-functions)
> -   run your tests with gitlab-ci
> -   consult your detailled results with allure

All those steps are detailed [here](https://essensium-mind.gitlab.io/mestr/quickstart.html)
