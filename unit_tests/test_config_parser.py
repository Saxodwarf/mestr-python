from unittest import mock
from tempfile import mkstemp
from typing import Dict
from mestr import config_parser

_Manager = [
    "Managers : ",
    "    -",
    "        type: test  ",
    "        name : default "
    ]


def write_default_man():
        fd_man, man_tmp = mkstemp()
        with open(man_tmp, 'w') as f:
            f.write("\n".join(_Manager))
            f.seek(0)
        return man_tmp


def test_yaml_config_default_manager_only():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        man_tmp = write_default_man()
        dct: Dict = config_parser.yaml_config(None, man_tmp)
        assert "managers" in dct
        assert "" in dct["Features"]
        assert "default" in dct["managers"]


def test_parse_reports_with_formatter_only():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        seq = ["Reports:",
               "    formatter : pretty"]
        fd_url, url_tmp = mkstemp()
        fd_man, man_tmp = mkstemp()
        with open(url_tmp, 'w') as f:
            f.write("\n".join(seq))
            f.seek(0)
        man_tmp = write_default_man()
        dct: Dict = config_parser.yaml_config(url_tmp, man_tmp)
        assert "Formatter" in dct
        assert dct["Formatter"] == "pretty"


def test_parse_reports_with_existing_directory():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        seq = ["Reports:",
               "    formatter : pretty",
               "    output : mestr/managers"]
        fd_url, url_tmp = mkstemp() 
        with open(url_tmp, 'w') as f:
            f.write("\n".join(seq))
            f.seek(0)
        man_tmp = write_default_man()
        dct: Dict = config_parser.yaml_config(url_tmp, man_tmp)
        assert "Formatter" in dct
        assert "Output" in dct
        assert "Log_files" in dct
        assert dct["Formatter"] == 'pretty'
        assert dct["Output"] == 'mestr/managers'
        assert dct["Log_files"] == "mestr/managers/"


def test_parse_reports_with_existing_directory_and_slash():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        seq = ["Reports:",
               "    formatter : pretty",
               "    output : mestr/managers/"]
        fd_url, url_tmp = mkstemp()
        with open(url_tmp, 'w') as f:
            f.write("\n".join(seq))
            f.seek(0)
        man_tmp = write_default_man()
        dct: Dict = config_parser.yaml_config(url_tmp, man_tmp)
        assert "Formatter" in dct
        assert "Output" in dct
        assert "Log_files" in dct
        assert dct["Formatter"] == 'pretty'
        assert dct["Output"] == 'mestr/managers/'
        assert dct["Log_files"] == "mestr/managers/"


def test_parse_reports_with_not_existing_directory():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        seq = ["Reports:",
               "    formatter : pretty",
               "    output : mestr/unicycle/"]
        fd_url, url_tmp = mkstemp()
        with open(url_tmp, 'w') as f:
            f.write("\n".join(seq))
            f.seek(0)
        man_tmp = write_default_man()
        dct: Dict = config_parser.yaml_config(url_tmp, man_tmp)
        assert "Formatter" in dct
        assert "Output" in dct
        assert "Log_files" in dct
        assert dct["Formatter"] == 'pretty'
        assert dct["Output"] == 'mestr/unicycle/'
        assert dct["Log_files"] == "mestr/unicycle/"


def test_parse_reports_with_not_existing_file():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        seq = ["Reports:",
               "    formatter : pretty",
               "    output : mestr/unicycle"]
        fd_url, url_tmp = mkstemp()
        with open(url_tmp, 'w') as f:
            f.write("\n".join(seq))
            f.seek(0)
        man_tmp = write_default_man()
        dct: Dict = config_parser.yaml_config(url_tmp, man_tmp)
        assert "Formatter" in dct
        assert "Output" in dct
        assert "Log_files" in dct
        assert dct["Formatter"] == 'pretty'
        assert dct["Output"] == 'mestr/unicycle'
        assert dct["Log_files"] == "mestr/"


def test_parse_reports_with_existing_file():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        seq = ["Reports:",
               "    formatter : pretty",
               "    output : mestr/config_parser.py"]
        fd_url, url_tmp = mkstemp()
        with open(url_tmp, 'w') as f:
            f.write("\n".join(seq))
            f.seek(0)
        man_tmp = write_default_man()
        dct: Dict = config_parser.yaml_config(url_tmp, man_tmp)
        assert "Formatter" in dct
        assert "Output" in dct
        assert "Log_files" in dct
        assert dct["Formatter"] == 'pretty'
        assert dct["Output"] == 'mestr/config_parser.py'
        assert dct["Log_files"] == "mestr/"


def test_parse_reports_with_file_only():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        seq = ["Reports:",
               "    output : mestr/unicycle.log"]
        fd_url, url_tmp = mkstemp()
        with open(url_tmp, 'w') as f:
            f.write("\n".join(seq))
            f.seek(0)
        man_tmp = write_default_man()
        dct: Dict = config_parser.yaml_config(url_tmp, man_tmp)
        assert "Log_files" in dct
        assert "Output" in dct
        assert dct["Output"] == 'mestr/unicycle.log'
        assert dct["Log_files"] == "mestr/"


def test_parse_tags_with_directory_only():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        seq = ["Reports:",
               "    output : mestr/unicycle/"]
        fd_url, url_tmp = mkstemp()
        with open(url_tmp, 'w') as f:
            f.write("\n".join(seq))
            f.seek(0)
        man_tmp = write_default_man()
        dct: Dict = config_parser.yaml_config(url_tmp, man_tmp)
        assert "Formatter" in dct
        assert '' in dct["Formatter"]
        assert "Log_files" in dct
        assert "mestr/" in dct["Log_files"]


def test_parse_tags_tag_or_not_tag():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        seq = ["Tags:",
               "   - tobe, ~tobe"]
        fd_url, url_tmp = mkstemp()
        with open(url_tmp, 'w') as f:
            f.write("\n".join(seq))
            f.seek(0)
        man_tmp = write_default_man()
        dct: Dict = config_parser.yaml_config(url_tmp, man_tmp)
        assert "Tags" in dct
        assert ["tobe", "~tobe"] in dct["Tags"]


def test_parse_tags_tag_and_tag():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        seq = ["Tags:",
               "   - bed",
               "   - breakfeast"]
        fd_url, url_tmp = mkstemp()
        with open(url_tmp, 'w') as f:
            f.write("\n".join(seq))
            f.seek(0)
        man_tmp = write_default_man()
        dct: Dict = config_parser.yaml_config(url_tmp, man_tmp)
        assert "Tags" in dct
        assert ["bed"] in dct["Tags"]
        assert ["breakfeast"] in dct["Tags"]


def test_parse_filters_include():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        seq = ["Filter :",
               "   include : 'toto'"]
        fd_url, url_tmp = mkstemp()
        with open(url_tmp, 'w') as f:
            f.write("\n".join(seq))
            f.seek(0)
        man_tmp = write_default_man()
        dct: Dict = config_parser.yaml_config(url_tmp, man_tmp)
        assert "Filter" in dct
        assert "toto" == dct["Filter"]["include"]


def test_parse_filters_exclude():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        seq = ["Filter :",
               "   exclude : 'tata'"]
        fd_url, url_tmp = mkstemp()
        with open(url_tmp, 'w') as f:
            f.write("\n".join(seq))
            f.seek(0)
        man_tmp = write_default_man()
        dct: Dict = config_parser.yaml_config(url_tmp, man_tmp)
        assert "Filter" in dct
        assert "tata" == dct["Filter"]["exclude"]


def test_parse_filters_both():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        seq = ["Filter :",
               "   include : 'toto'",
               "   exclude : 'tata'"]
        fd_url, url_tmp = mkstemp()
        with open(url_tmp, 'w') as f:
            f.write("\n".join(seq))
            f.seek(0)
        man_tmp = write_default_man()
        dct: Dict = config_parser.yaml_config(url_tmp, man_tmp)
        assert "Filter" in dct
        assert "toto" == dct["Filter"]["include"]
        assert "tata" == dct["Filter"]["exclude"]


def test_parse_Features_Stage_Integrity():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        seq = ["Features: my-project/features/",
               "Stage: unicycle"]
        fd_url, url_tmp = mkstemp()
        with open(url_tmp, 'w') as f:
            f.write("\n".join(seq))
            f.seek(0)
        man_tmp = write_default_man()
        dct: Dict = config_parser.yaml_config(url_tmp, man_tmp)
        assert "Stage" in dct, "stage not in data"
        assert "Features" in dct, "features not in data"
        assert "my-project/features/" in dct["Features"], "wrong features and/or integrity str"
        assert "unicycle" in dct["Stage"]


def test_custom_manager_path():
    with mock.patch("mestr.config_parser.list_managers") as lst_man:
        lst_man.return_value = mock.MagicMock()
        fd_url, url_tmp = mkstemp()
        man_tmp = write_default_man()
        seq = ["Managers: " + man_tmp]
        with open(url_tmp, 'w') as f:
            f.write("\n".join(seq))
            f.seek(0)
        dct: Dict = config_parser.yaml_config(url_tmp, None)
        assert "managers" in dct
        assert "" in dct["Features"]
        assert "default" in dct["managers"]
