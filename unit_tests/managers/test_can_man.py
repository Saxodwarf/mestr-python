"""Unit tests for the GPIO manager."""

import time
import re
import socket
import unittest.mock
import pytest
from mestr.managers import can_man
from mestr.utils.exceptions import MestrTimeout
from unit_tests.managers import test_manager


# pylint: disable=unused-argument
def side_effect_bind(adress)->None:
    """Side effect function : raises an exception if a specific pin name is
    given, to mimic the behavior of the actual library.
    """
    raise OSError("boooooh")


def side_effect_recv(bufsize,
                     sleep_time,
                     packet="1fce0a80080000000123456789abcdef",
                     does_timeout=False):
    if does_timeout:
        raise socket.timeout
    time.sleep(sleep_time)
    return bytes.fromhex(packet)


can_man.socket = unittest.mock.create_autospec(socket)
can_man.check_call = unittest.mock.create_autospec(can_man.check_call)
can_man.socket.CAN_EFF_MASK = socket.CAN_EFF_MASK
can_man.socket.CAN_EFF_FLAG = socket.CAN_EFF_FLAG


class TestCanManager(test_manager._TestManagerApi):
    """Test class for the GPIO manager."""
    iface = "can1"
    manager = can_man.CanManager(iface)
    _man_type = "CAN"

    def test_creating_the_object_with_default_value(self)->None:
        """Test if the default attribute are correct"""
        can_man.socket.reset_mock()
        manager = can_man.CanManager(self.iface)
        can_man.socket.socket.assert_called_with(can_man.socket.PF_CAN,
                                                 can_man.socket.SOCK_RAW,
                                                 can_man.socket.CAN_RAW)
        manager._socket.bind.assert_called_with((self.iface,))
        manager._socket.settimeout.assert_called_with(60)

    def test_creating_the_object_with_bad_iface(self)->None:
        """Test if the correct exception is raised when an unimplemented
        interface is used.
        """
        can_man.socket.reset_mock()
        with pytest.raises(NotImplementedError):
            manager = can_man.CanManager("can25")

    def test_creating_the_object_with_failing_bind(self, capsys)->None:
        """Test the behavior when the interface could not be bound to the socket.
        """
        can_man.socket.reset_mock()
        with unittest.mock.patch("mestr.managers.can_man.socket",
                                 spec=can_man.socket) as m_socket:
            m_socket.socket().bind.side_effect = side_effect_bind
            with pytest.raises(OSError):
                man = can_man.CanManager(self.iface)
            captured = capsys.readouterr()
            assert captured.out == ("Could not bind to interface " +
                                    self.iface + "\n")

    @pytest.mark.parametrize(["packet", "can_id", "data"],
                             [
                                 (bytes.fromhex(
                                     "ff070000020000001234000000000000"),
                                  int("7ff", 16),        # High id limit
                                  b"\x12\x34"),          # for SFF can
                                 (bytes.fromhex(
                                     "45010000020000001234000000000000"),
                                  int("145", 16),        # Middle range id
                                  b"\x12\x34"),          # value for SFF can
                                 (bytes.fromhex(
                                     "00000000020000001234000000000000"),
                                  int("0", 16),          # Low id limit
                                  b"\x12\x34"),
                                 (bytes.fromhex(
                                     "00800080020000001234000000000000"),
                                  int("8000", 16),       # First id that needs
                                  b"\x12\x34"),          # EFF
                                 (bytes.fromhex(
                                     "1fce0080040000001234eacb00000000"),
                                  int("ce1f", 16),       # Middle rande id,
                                  b"\x12\x34\xea\xcb"),  # and longer payload
                                 (bytes.fromhex(
                                     "1fce0a80080000000123456789abcdef"),
                                  int("ace1f", 16),       # Middle rande id,
                                  # and longest possible payload
                                  b"\x01\x23\x45\x67\x89\xab\xcd\xef"),
                                 (bytes.fromhex(
                                     "ffffff9f020000001234000000000000"),
                                  int("1fffffff", 16),   # High id limit for
                                  b"\x12\x34"),          # EFF CAN
                             ])
    def test__send_with_good_ids(self, packet, can_id, data)->None:
        """Test the _send method"""
        can_man.socket.reset_mock()
        man = can_man.CanManager(self.iface)
        man._send(can_id, data)
        man._socket.send.assert_called_with(packet)

    @pytest.mark.parametrize(["can_id", "data"],
                             [
                                 (int("20000000", 16),  # Just above the id
                                  b"\x12\x34"),         # limit for EFF can
                                 (int("ce1f", 16),  # Too long payload
                                  b"\x01\x23\x45\x67\x89\xab\xcd\xef\x12"),
                             ])
    def test__send_with_bad_values(self, can_id, data)->None:
        """Test the _send method"""
        can_man.socket.reset_mock()
        man = can_man.CanManager(self.iface)
        with pytest.raises(ValueError):
            man._send(can_id, data)

    @pytest.mark.parametrize(["packet", "str_pckt"],
                             [
                                 (bytes.fromhex(
                                     "ff070000020000001234000000000000"),
                                  "7ff#1234"),  # High id limit for SFF can
                                 (bytes.fromhex(
                                     "45010000020000001234000000000000"),
                                  "145#1234"),  # Middle range id value
                                                # for SFF can
                                 (bytes.fromhex(
                                     "00000000020000001234000000000000"),
                                  "0#1234"),    # Low id limit
                                 (bytes.fromhex(
                                     "00800080020000001234000000000000"),
                                  "8000#1234"),  # First id that needs EFF
                                 (bytes.fromhex(
                                     "1fce0080040000001234eacb00000000"),
                                  "ce1f#1234eacb"),  # Middle rande id,
                                                     # and longer payload
                                 (bytes.fromhex(
                                     "1fce0a80080000000123456789abcdef"),
                                  "ace1f#0123456789abcdef"),
                                 # Middle rande id and longest possible payload
                                 (bytes.fromhex(
                                     "ffffff9f020000001234000000000000"),
                                  "1fffffff#1234"),  # High id limit for
                                                     # EFF CAN
                             ])
    def test_send_valid_string(self, str_pckt, packet):
        """Test the higher level send method"""
        can_man.socket.reset_mock()
        man = can_man.CanManager(self.iface)
        man.send(str_pckt)
        man._socket.send.assert_called_with(packet)

    @pytest.mark.parametrize(["str_pckt", "err_msg"],
                             [
                                 ("20000000#1234",     # Just above the id
                                  "Too long can ID"),  # limit for EFF can
                                 ("#1234",         # No CAN id
                                  "Invalid format: missing CAN id"),
                                 ("123#",          # No data
                                  "Invalid format: missing data"),
                                 ("",              # Empty string
                                  "Invalid format: missing CAN id"),
                                 ("123abcdef",     # No separator
                                  "Invalid format: missing '#' separator"),
                                 ("1y23#abcdef",   # Non hexadecimal CAN id
                                  "invalid literal for int() with base 16"),
                                 ("123#abcdzef",   # Non hexadecimal CAN id
                                  "non-hexadecimal number found"),
                                 ("ce1f#0123456789abcdef1234",
                                  # Too long payload
                                  "data is too long for a CAN frame"),
                             ])
    def test_send_with_bad_values(self, capsys, str_pckt, err_msg)->None:
        """Test the _send method"""
        can_man.socket.reset_mock()
        man = can_man.CanManager(self.iface)
        with pytest.raises(ValueError) as exc_info:
            man.send(str_pckt)
        assert err_msg in str(exc_info.value)

    @pytest.mark.parametrize(["packet", "can_id", "data"],
                             [
                                 (bytes.fromhex(
                                     "bc0a0000020000001234000000000000"),
                                  int("2BC", 16),  # ID above the limit,
                                  b"\x12\x34"),    # truncating
                                 (bytes.fromhex(
                                     "ff070000020000001234000000000000"),
                                  int("7ff", 16),  # High id limit for SFF can
                                  b"\x12\x34"),
                                 (bytes.fromhex(
                                     "00080000020000001234000000000000"),
                                  int("0", 16),  # Truncating the id just above
                                  b"\x12\x34"),  # the limit
                                 (bytes.fromhex(
                                     "ff0f0080020000001234000000000000"),
                                  int("fff", 16),  # Middle range id value
                                  b"\x12\x34"),    # for EFF can
                                 (bytes.fromhex(
                                     "00000000020000001234000000000000"),
                                  int("0", 16),  # Low id limit for SFF can
                                  b"\x12\x34"),
                                 (bytes.fromhex(
                                     "ffffff9f020000001234000000000000"),
                                  int("1fffffff", 16),  # High id limit for
                                  b"\x12\x34"),         # EFF CAN
                                 (bytes.fromhex(
                                     "00000080020000001234000000000000"),
                                  int("0", 16),  # Low id limit for EFF can
                                  b"\x12\x34"),
                                 (bytes.fromhex(
                                     "1fce0080040000001234eacb00000000"),
                                  int("ce1f", 16),       # Middle rande id,
                                  b"\x12\x34\xea\xcb"),  # and longer payload
                                 (bytes.fromhex(
                                     "1fce0080020000001234eacb00000000"),
                                  int("ce1f", 16),  # Long payload with shorter
                                  b"\x12\x34"),     # data length
                             ])
    def test_recv(self, packet, can_id, data)->None:
        """Test test the recv method"""
        can_man.socket.reset_mock()
        can_man.socket.socket().recv.return_value = packet
        man = can_man.CanManager(self.iface)
        pckt = man.recv()
        man._socket.recv.assert_called_with(16)
        assert pckt[0] == can_id
        assert pckt[1] == data

    @pytest.mark.parametrize(["packet", "can_id", "data"],
                             [
                                 (bytes.fromhex(
                                     "ff070000020000001234000000000000"),
                                  int("7ff", 16),        # High id limit
                                  b"\x12\x34"),          # for SFF can
                                 (bytes.fromhex(
                                     "45010000020000001234000000000000"),
                                  int("145", 16),        # Middle range id
                                  b"\x12\x34"),          # value for SFF can
                                 (bytes.fromhex(
                                     "00000000020000001234000000000000"),
                                  int("0", 16),          # Low id limit
                                  b"\x12\x34"),
                                 (bytes.fromhex(
                                     "00800080020000001234000000000000"),
                                  int("8000", 16),       # First id that needs
                                  b"\x12\x34"),          # EFF
                                 (bytes.fromhex(
                                     "1fce0080040000001234eacb00000000"),
                                  int("ce1f", 16),       # Middle rande id,
                                  b"\x12\x34\xea\xcb"),  # and longer payload
                                 (bytes.fromhex(
                                     "1fce0a80080000000123456789abcdef"),
                                  int("ace1f", 16),       # Middle rande id,
                                  # and longest possible payload
                                  b"\x01\x23\x45\x67\x89\xab\xcd\xef"),
                                 (bytes.fromhex(
                                     "ffffff9f020000001234000000000000"),
                                  int("1fffffff", 16),   # High id limit for
                                  b"\x12\x34"),          # EFF CAN
                             ])
    def test_expect_everything_correct(self,
                                       packet,
                                       can_id,
                                       data)->None:
        """Test the expect method when everythong works fine.
        """
        can_man.socket.reset_mock()
        with unittest.mock.patch("mestr.managers.can_man.socket",
                                 spec=can_man.socket) as m_socket:
            m_socket.timeout = socket.timeout
            m_socket.CAN_EFF_MASK = socket.CAN_EFF_MASK
            m_socket.CAN_EFF_FLAG = socket.CAN_EFF_FLAG
            m_socket.socket().recv.side_effect = (lambda bufsize:
                                                  side_effect_recv(
                                                      bufsize,
                                                      1,
                                                      packet.hex()))
            man = can_man.CanManager(self.iface)
            man.expect(data, can_id, 5)

    @pytest.mark.parametrize(["latency", "packet", "can_id", "data"],
                             [
                                 (1,
                                  bytes.fromhex(
                                      "ff070000020000001234000000000000"),
                                  int("7ff", 16),  # Packet doesn't match
                                  b"\x13\x35"),    # because of wrong data
                                 (1,
                                  bytes.fromhex(
                                      "45010000020000001234000000000000"),
                                  int("146", 16),  # Packet doesn't match
                                  b"\x12\x34"),    # because of wrong id
                                 (100,
                                  bytes.fromhex(
                                      "45010000020000001234000000000000"),
                                  int("145", 16),  # Packet arrives too late
                                  b"\x12\x34")
                             ])
    def test_expect_global_timeout(self,
                                   latency,
                                   packet,
                                   can_id,
                                   data)->None:
        """Test the expect method when the global timeout is shorter than the
        socket timeout.
        """
        can_man.socket.reset_mock()
        with unittest.mock.patch("mestr.managers.can_man.socket",
                                 spec=can_man.socket) as m_socket:
            m_socket.timeout = socket.timeout
            m_socket.CAN_EFF_MASK = socket.CAN_EFF_MASK
            m_socket.CAN_EFF_FLAG = socket.CAN_EFF_FLAG
            m_socket.socket().recv.side_effect = (lambda bufsize:
                                                  side_effect_recv(
                                                      bufsize,
                                                      latency,
                                                      packet.hex()))
            man = can_man.CanManager(self.iface, timeout=2, sock_timeout=10)
            with pytest.raises(MestrTimeout):
                man.expect(data, can_id)

    def test_expect_sock_timeout(self)->None:
        """Test the expect method when the socket timeout is shorter than the
        global timeout.
        """
        can_man.socket.reset_mock()
        with unittest.mock.patch("mestr.managers.can_man.socket",
                                 spec=can_man.socket) as m_socket:
            m_socket.timeout = socket.timeout
            m_socket.CAN_EFF_MASK = socket.CAN_EFF_MASK
            m_socket.CAN_EFF_FLAG = socket.CAN_EFF_FLAG
            packet = "ff070000020000001234000000000000"
            latency = 100
            can_id = int("7ff", 16)
            data = b"\x13\x35"
            m_socket.socket().recv.side_effect = (lambda bufsize:
                                                  side_effect_recv(
                                                      bufsize,
                                                      latency,
                                                      packet,
                                                      True))
            man = can_man.CanManager(self.iface, timeout=10, sock_timeout=2)
            man._socket.settimeout.assert_called_with(2)
            with pytest.raises(MestrTimeout):
                man.expect(data, can_id)

    @pytest.mark.parametrize(["latency", "packet", "can_id", "data", "ret_val"],
                             [
                                 (1,
                                  "45010000020000001234000000000000",
                                  int("145", 16),  # full actual data
                                  b"\x12\x34",
                                  0),
                                 (1,
                                  "ff070000020000001234000000000000",
                                  int("7ff", 16),  # Data #2 matches
                                  [b"\x13\x34",
                                   b"\x18\x34",
                                   b"\x12\x34"],
                                  2),
                                 (1,
                                  "45010000020000001234000000000000",
                                  int("145", 16),  # full data regex
                                  re.compile(b"\x12\x34"),
                                  0),
                                 (1,
                                  "45010000020000001234000000000000",
                                  int("145", 16),  # Partial data regex
                                  re.compile(b"\x34"),
                                  0),
                                 (1,
                                  "45010000020000001234000000000000",
                                  int("145", 16),  # The first data that can
                                  [re.compile(b"\xff"),  # match is used, even
                                   re.compile(b"\x34"),  # if an other, more
                                   re.compile(b"\x12\x34"),  # complete regex
                                   re.compile(b"\x12\x34")   # is in the list.
                                   ],
                                  1),
                                 (1,
                                  "45010000020000001234000000000000",
                                  int("145", 16),  # Bytes string and regex
                                  [b"\xff",        # pattern can be used in
                                   re.compile(b"\xb4"),  # the same list
                                   b"\x12\x38",
                                   re.compile(b"[\x01-\xf0]")
                                   ],
                                  3),
                             ])
    def test_expect_data_type(self, latency, packet, can_id, data, ret_val):
        can_man.socket.reset_mock()
        with unittest.mock.patch("mestr.managers.can_man.socket",
                                 spec=can_man.socket) as m_socket:
            m_socket.timeout = socket.timeout
            m_socket.CAN_EFF_MASK = socket.CAN_EFF_MASK
            m_socket.CAN_EFF_FLAG = socket.CAN_EFF_FLAG
            m_socket.socket().recv.side_effect = (lambda bufsize:
                                                  side_effect_recv(
                                                      bufsize,
                                                      latency,
                                                      packet))
            man = can_man.CanManager(self.iface)
            ret = man.expect(data, can_id, 10)
            assert ret == ret_val
