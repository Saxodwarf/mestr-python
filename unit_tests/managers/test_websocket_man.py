from unittest import mock
from mestr.managers import websocket_man
from ..managers import test_manager


class TestWebManager(test_manager._TestManagerApi):

    manager = websocket_man.WebSocketManager()
    _man_type = "websocket"
    _initial_state = "closed"

    def test_dict_to_cookie(self):
        dct = {"unicycle": "amazing", "bike": "useless"}
        output = "unicycle=amazing ; bike=useless"
        assert websocket_man.dict_to_cookie(dct) == output

    def test_open_websocket_without_auth(self):
        with mock.patch('websocket.create_connection') as mock_method:
            self.manager.login(url="my.domain.test/websocket",
                               url_login="my.domain.test/login",
                               timeout=15)
            mock_method.assert_called_with('ws://my.domain.test/websocket',
                                           cookie='', headers={}, timeout=15)

    def test_open_websocket_with_auth(self):
        with mock.patch('mestr.managers.web_man.WebRequest.login') as mock_method:
            with mock.patch('websocket.create_connection') as mock_socket:
                self.manager.login(url="my.domain.test/websocket",
                                   url_login="my.domain.test/login",
                                   auth=("login", "password"),
                                   timeout=15)
                mock_method.assert_called_with()
                assert bool(mock_socket.call_args)
                args = ('ws://my.domain.test/websocket',)
                kwargs = {'cookie': '',
                          'headers': {'User-Agent': 'python-requests/2.20.1',
                                      'Accept-Encoding': 'gzip, deflate',
                                      'Accept': '*/*',
                                      'Connection': 'keep-alive'},
                          'timeout': 15}
                for arg in mock_socket.call_args[0]:
                    assert arg in args
                for arg in mock_socket.call_args[1]:
                    assert arg in kwargs
                    if arg != "headers":
                        assert mock_socket.call_args[1][arg] == kwargs[arg]
