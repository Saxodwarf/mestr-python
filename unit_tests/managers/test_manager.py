#from mestr.managers import manager
import pytest


class _TestManagerApi:
    """ base class designed to be used to test managers subclasses compliance
    with the api
    """
    manager = None
    _man_type = "Manager"
    _initial_state = "no State"
    _not_implemented_state = "not implemented state"

    def test_logging(self):
        self.manager.start_logging("test_file")
        f = self.manager.stop_logging()
        if isinstance(f, str):
            assert f == "test_file"
        else:
            assert "test_file" in f

    def test_man_type(self):
        assert self._man_type == self.manager.man_type()

    def test_initial_state(self):
        assert self.manager.is_state(self._initial_state)

    def test_not_implemented_state(self):
        with pytest.raises(ValueError) as e_info:
            assert self.manager.is_state(self._not_implemented_state)
