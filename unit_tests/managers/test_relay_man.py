"""Unit tests for the GPIO manager."""

import unittest.mock
import pytest

from mestr.managers import relay_man
from mestr.managers.gpio_man import GpioManager
from unit_tests.managers import test_manager
from unit_tests.stubs import GPIO_stub as GPIO


# pylint: disable=unused-argument
def side_effect_GpioManager(gpio, direction, mode, value=0)->None:
    """Side effect function : raises an exception if a specific pin name is
    given, to mimic the behavior of the actual library.
    """
    if gpio == "WRONG_PIN":
        msg = "GPIO {} doesn't exist".format(gpio)
        raise ValueError(msg)
    return unittest.mock.DEFAULT

relay_man.GpioManager = unittest.mock.Mock(spec=GpioManager)

# - Input
relay_man.GpioManager.IN = 0
# - Output
relay_man.GpioManager.OUT = 1

# Active
# Active at low logical level, state is High when reading 0
relay_man.GpioManager.ACT_LOW = 0
# Active at high logical level, state is High when reading 1
relay_man.GpioManager.ACT_HIGH = 1

# State :
# High, logic 1
relay_man.GpioManager.HIGH = 1
# Low, logic 0
relay_man.GpioManager.LOW = 0
relay_man.GpioManager.side_effect = side_effect_GpioManager


#@pytest.fixture
#def mock_GpioManager():
    #yield
    #relay_man.GpioManager.side_effect = 

IN = 0
# - Output
OUT = 1
# Active
# Active at low logical level, state is High when reading 0
ACT_LOW = 0
# Active at high logical level, state is High when reading 1
ACT_HIGH = 1

# State :
# High, logic 1
HIGH = 1
# Low, logic 0
LOW = 0


class TestRelayManager(test_manager._TestManagerApi):
    """Test class for the GPIO manager."""
    _pin = "P8_10"
    manager = relay_man.RelayManager(_pin)
    _man_type = "relay"
    _initial_state = "off"

    def test_creating_the_object_with_default_value(self)->None:
        """Test if the default attribute are correct"""
        relay_man.GpioManager.reset_mock()
        man = relay_man.RelayManager(self._pin)
        print(man.gpio)
        relay_man.GpioManager.assert_called_with(gpio=self._pin,
                                                 direction=OUT,
                                                 mode=ACT_LOW,
                                                 value=0)

    @pytest.mark.parametrize(["value", "mode"],
                             [
                                 (0, relay_man.RelayManager.ACT_HIGH),
                                 (1, relay_man.RelayManager.ACT_HIGH),
                                 (0, relay_man.RelayManager.ACT_LOW),
                                 (1, relay_man.RelayManager.ACT_LOW)
                             ])
    def test_creating_the_object_with_non_default_value(self,
                                                        value,
                                                        mode)->None:
        """Test if the non default attribute are passed correctly"""
        relay_man.GpioManager.reset_mock()
        relay_man.RelayManager(self._pin,
                               mode=mode,
                               value=value)
        relay_man.GpioManager.assert_called_with(self._pin,
                                                 OUT,
                                                 mode=mode,
                                                 value=value)

    @pytest.mark.parametrize(["value", "mode"],
                             [
                                 (0, "ACT_HIGH"),
                                 (1, "ACT_HIGH"),
                                 (0, "ACT_LOW"),
                                 (1, "ACT_LOW")
                             ])
    def test_creating_the_object_with_text_value(self, value, mode)->None:
        """Test if the object behaves correctly when given a text argument
        instead of a constant.
        """
        relay_man.GpioManager.reset_mock()
        relay_man.RelayManager(self._pin, mode=mode, value=value)
        relay_man.GpioManager.assert_called_with(self._pin, direction=OUT,
                                                 mode=mode,
                                                 value=value)

    def test_creating_the_object_with_bad_pin(self)->None:
        """Test if the correct exception is raised when an error occurs
        on pin setup.
        """
        with pytest.raises(ValueError):
            relay_man.RelayManager("WRONG_PIN")
