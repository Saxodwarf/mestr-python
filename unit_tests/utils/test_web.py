import unittest.mock
from urllib.parse import urlsplit
import pytest
from mestr.utils import web


def test_create_url_full_with_default_arguments():
    url = urlsplit("http://my.test.do/foo/bar?foo=foo&bar=bar#amazing unicycle")
    assert url == web.create_url("http://my.test.do/foo/bar?foo=foo&bar=bar#amazing unicycle",
                                 scheme="proc", netloc="my.domain.ex",
                                 path="/toto/titi", query="toto=toto&titi=titi",
                                 fragment="default fragment")


def test_create_url_without_netloc_and_scheme_with_default_arguments():
    url = urlsplit("proc://my.domain.ex/foo/bar?foo=foo&bar=bar#amazing unicycle")
    assert url == web.create_url("/foo/bar?foo=foo&bar=bar#amazing unicycle",
                                 scheme="proc", netloc="my.domain.ex",
                                 path="/toto/titi", query="toto=toto&titi=titi",
                                 fragment="default fragment")


def test_create_url_without_scheme_with_default_arguments():
    url = urlsplit("proc://my.test.do/foo/bar?foo=foo&bar=bar#amazing unicycle")
    assert url == web.create_url("my.test.do/foo/bar?foo=foo&bar=bar#amazing unicycle",
                                 scheme="proc", netloc="my.domain.ex",
                                 path="/toto/titi", query="toto=toto&titi=titi",
                                 fragment="default fragment")


def test_create_empty_url_with_default_arguments():
    url = urlsplit("proc://my.domain.ex/toto/titi?toto=toto&titi=titi#default fragment")
    assert url == web.create_url("",
                                 scheme="proc", netloc="my.domain.ex",
                                 path="/toto/titi", query="toto=toto&titi=titi",
                                 fragment="default fragment")


def test_create_url_with_wrong_argument():
    with pytest.raises(KeyError) as e_info:
        url = urlsplit("proc://my.domain.ex/toto/titi?toto=toto&titi=titi#default fragment")
        assert url == web.create_url("", unicycle="amazing")


# update url
def test_update_url_with_wrong_argument():
    with pytest.raises(TypeError) as e_info:
        url = urlsplit("proc://my.domain.ex/toto/titi?toto=toto&titi=titi#default fragment")
        assert url == web.update_url(urlsplit(""), unicycle="amazing")


def test_update_url_with_full_default_arguments():
    url = urlsplit("proc://my.domain.ex/toto/titi?toto=toto&titi=titi#default fragment")
    assert url == web.update_url(urlsplit("my.test.do/foo/bar?foo=foo&bar=bar#amazing unicycle"),
                                 scheme="proc", netloc="my.domain.ex",
                                 path="/toto/titi", query="toto=toto&titi=titi",
                                 fragment="default fragment")


def test_update_url_with_some_default_arguments():
    url = urlsplit("proc://my.test.do/foo/bar?toto=toto&titi=titi#default fragment")
    assert url == web.update_url(urlsplit("http://my.test.do/foo/bar?foo=foo&bar=bar#amazing unicycle"),
                                 scheme="proc", query="toto=toto&titi=titi",
                                 fragment="default fragment")


def test_create_url_without_default_arguments():
    url = urlsplit("proc://my.test.do/foo/bar?toto=toto&titi=titi#default fragment")
    assert url == web.update_url(urlsplit("proc://my.test.do/foo/bar?toto=toto&titi=titi#default fragment"))


# parse class
def test_parse_classe_basic():
    assert isinstance(web.parse_class("unittest.mock.Mock()"),
                      unittest.mock.Mock)


def test_parse_classe_list_argument():
    assert isinstance(web.parse_class("unittest.mock.Mock('unicycle')"),
                      str)


def test_parse_classe_dict_arguments():
    test = web.parse_class("unittest.mock.Mock(unicycle='amazing'")
    assert test.unicycle == "amazing"
