#!/bin/bash

function usage() {
    printf "This script configures routing between 2 interfaces.\\n"
    printf "Usage :\\n"
    printf "\\t %s -w wanInterface -l lanInterface [-pn]\\n" "$0"
    printf "\\t \\e[4mwanInterface\\e[0m is the interface connected to the wan.\\n"
    printf "\\t \\e[4mlanInterface\\e[0m is the interface connected to the lan.\\n"
    printf "\\t \\e[4m-c\\e[0m clear the existing iptables rules\\n"
    printf "\\t \\e[4m-p\\e[0m makes the change persistent\\n"
    printf "\\t \\e[4m-n\\e[0m prevent the installation of iptables-persistent. (It prevents having to wait for apt-get update to finish for nothing if the package is already installed.)\\n\\n"
    printf "As you need root priviledges to change routing table configurations, this script must be run as root.\\n"
}

function is_ip_forward_not_persistent() {
    # To know if IP forwarding is already activated in a persistent way,
    # we look for the net.ipv4.ip_forward parameter in the sysctl.conf file.
    # If we find it uncommented, we consider it to be activated.
    grep_op="$(grep -e "net.ipv4.ip_forward=1" /etc/sysctl.conf)"
    lnnm=0
    for line in $grep_op; do
        if [ "$line" == "net.ipv4.ip_forward=1" ]; then
            lnnm=$(( lnnm + 1 ))
        fi
    done
    (( lnnm == 0 ))
}

is_root() {
    if [ "$EUID" -ne 0 ]; then
        echo "Please run as root"
        exit 1
    fi
}

wan=""
lan=""
persistent=0
clear_rules=0
install=1

if ! args=$(getopt -o phncw:l: -- "$@"); then
    echo "Incorrect options provided"
    exit 1
fi

eval set -- "$args"

while [ $# -ge 1 ]; do
    case "$1" in
    --)
        shift
        break
        ;;
    -w)
        wan="$2"
        shift
        ;;
    -l)
        lan="$2"
        shift
        ;;
    -p)
        persistent=1
        ;;
    -c)
        clear_rules=1
        ;;
    -n)
        install=0
        ;;
    -h)
        usage
        exit 0
        ;;
    esac

    shift
done

is_root
if [[ $lan == "" ]] || [[ $wan == "" ]]; then
    echo "The lan and wan interfaces must be specified."
    usage
    exit 0
fi
if [ $clear_rules == 1 ]; then
    iptables --flush
    iptables -t nat --flush
fi
## For a non persistent connexion :
sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
iptables -A FORWARD -i "$lan" -o "$wan" -j ACCEPT
iptables -A FORWARD -i "$wan" -o "$lan" -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -t nat -A POSTROUTING -o "$wan" -j MASQUERADE

## To make it persistant :
if [ $persistent == 1 ]; then
    if is_ip_forward_not_persistent; then
        echo "net.ipv4.ip_forward=1" >>/etc/sysctl.conf
    fi
    if [ $install = 1 ]; then
        apt-get -y update
        apt-get -y install iptables-persistent
    fi
    iptables-save >/etc/iptables/rules.v4
fi
